from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.admin.models import LogEntry, DELETION
from django.utils.html import escape
from django.urls import reverse
from django.utils.safestring import mark_safe
from simple_history.admin import SimpleHistoryAdmin

from .models import Competition, MedicalExamination, Payment, Registration, CompetitionLength, CompetitionVenue, \
    Brigade, MoneyToBrigadeRate, MembershipFees, BrigadeWork, Person, FulfilledBrigade, FulfilledMembership, \
    InternalLog, ChampionshipCourseRule, Transport


class PersonInline(admin.StackedInline):
    model = Person
    can_delete = False
    verbose_name_plural = 'persons'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (PersonInline,)
    ordering = ('last_name', 'first_name',)
    list_display = ('username', "last_name", "first_name")

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


# Register your models here.

@admin.register(Person)
class PersonAdmin(SimpleHistoryAdmin):
    # list_display = ('title','created','updated',)
    search_fields = ['user__username',
                     'first_name__unaccent',
                     'last_name__unaccent']
    ordering = ('last_name', 'first_name',)
    list_display = ("last_name", "first_name", "birth_date", "member_since")


@admin.register(Competition)
class CompetitionAdmin(SimpleHistoryAdmin):
    ordering = ('-event_date',)


@admin.register(Transport)
class TransportAdmin(SimpleHistoryAdmin):
    list_display = ('competition', 'driver', 'get_passengers')

    def get_passengers(self, obj):
        return ", \n".join([str(p) for p in obj.passengers.all()])


@admin.register(MembershipFees)
class MembershipFeeAdmin(SimpleHistoryAdmin):
    ordering = ('-year', 'swimmerage_type',)


@admin.register(MoneyToBrigadeRate)
class MoneyToBrigadeRateAdmin(SimpleHistoryAdmin):
    ordering = ('-year',)


@admin.register(Payment)
class PaymentAdmin(SimpleHistoryAdmin):
    ordering = ('-date',)
    search_fields = ['person__last_name__unaccent',
                     'person__first_name__unaccent',
                     'due_date', 'date', 'membership_amount', 'brigade_amount']
    list_display = ('date', 'person', 'due_date', 'membership_amount', 'brigade_amount')


@admin.register(Registration)
class RegistrationAdmin(SimpleHistoryAdmin):
    list_display = ('competition', 'person', 'transport', 'championship_course')


@admin.register(MedicalExamination)
class MedicalExaminationAdmin(SimpleHistoryAdmin):
    ordering = ('person__last_name','person__first_name')
    list_display = ( 'person',
                    'examination_until',
                    'verified')
    search_fields = ['person__last_name__unaccent', 'person__first_name__unaccent']

@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'action_time'

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = '<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return mark_safe(link)
    object_link.admin_order_field = "object_repr"
    object_link.short_description = "object"

@admin.register(BrigadeWork)
class BrigadeWorkAdmin(SimpleHistoryAdmin):
    pass

@admin.register(CompetitionLength)
class CompetitionLengthAdmin(SimpleHistoryAdmin):
    pass

@admin.register(CompetitionVenue)
class CompetitionVenueAdmin(SimpleHistoryAdmin):
    pass

@admin.register(Brigade)
class BrigadeAdmin(SimpleHistoryAdmin):
    ordering = ('-date', 'person__last_name', 'person__first_name')
    search_fields = ['person__last_name__unaccent', 'person__first_name__unaccent', 'date__year']
    list_display = ('person',
                    'date',
                    'hours',
                    'work',
                    'fulfilled_differently',
                    'verified')


@admin.register(FulfilledBrigade)
class FulfilledBrigadeAdmin(SimpleHistoryAdmin):
    search_fields = ['person__last_name__unaccent',
                     'person__first_name__unaccent',
                     'year']
    list_display = [
        'person',
        'year',
        'fulfilled_brigade'
    ]


@admin.register(FulfilledMembership)
class FulfilledMembershipAdmin(SimpleHistoryAdmin):
    search_fields = ['person__last_name__unaccent',
                     'person__first_name__unaccent',
                     'year']
    list_display = [
        'person',
        'year',
        'fulfilled_membership'
    ]


@admin.register(InternalLog)
class InternalLogAdmin(SimpleHistoryAdmin):
    list_display = [
        'time_stamp',
        'category',
        'log_entry',
        'result',
    ]
    ordering = ('-time_stamp',)


@admin.register(ChampionshipCourseRule)
class ChampionshipCourseRuleAdmin(SimpleHistoryAdmin):
    list_display = [
        'swimmercategory',
        'temp_under4',
        'length',
    ]
