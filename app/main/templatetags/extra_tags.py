from django import template
from datetime import datetime

register = template.Library()


# tag to get field's verbose name in template
@register.simple_tag
def get_verbose_name(object):
    return object.verbose_name


# tag to get the value of a field by name in template
@register.simple_tag
def get_value_from_key(object, key):
    # is it necessary to check isinstance(object, dict) here?
    return object[key.name]


@register.simple_tag
def get_swimmerage_type(person):
    return person.get_swimmerage_type()

@register.simple_tag
def current_brigade_season():
    now = datetime.now()
    if now.month >= 7:
        return now.year
    else:
        return now.year - 1