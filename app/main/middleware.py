from threading import local

_request_local = local()

class CaptureRequestMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        _request_local.current_request = request
        response = self.get_response(request)
        return response