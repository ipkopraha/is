"""test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.urls import path, include, reverse_lazy, re_path

from . import views
from .views import ResetPasswordView

# from rest_framework import routers


app_name = "main"

urlpatterns = [
    path("", views.homepage, name="homepage"),
    path("accounts/", include('django.contrib.auth.urls')),
    path("user_info/", views.user_info, name="user_info"),
    path("aspirant_info/", views.aspirant_info, name="aspirant_info"),
    path("competitions/", views.list_competitions, name="list_competitions"),
    path("competition/<int:id>", views.list_attendees, name="list_attendees"),
    path("competition_public/<int:id>", views.list_attendees_public, name="list_attendees_public"),
    path("edit_transportation_admin/<int:id>", views.edit_transportation_admin, name="edit_transportation_admin"),
    path("transportation_publish/<int:id>", views.transportation_publish, name="transportation_publish"),
    path("transportation_generate/<int:id>", views.transportation_generate, name="transportation_generate"),
    path("transportation_lock_toggle/<int:id>", views.transportation_lock_toggle, name="transportation_lock_toggle"),
    path("transportation_remove_driver/<int:id>", views.transportation_remove_driver, name="transportation_remove_driver"),
    path("transportation_remove_passenger/<int:registration_id>", views.transportation_remove_passenger, name="transportation_remove_passenger"),
    path("clear_transportation/<int:id>", views.clear_transportation, name="clear_transportation"),
    path("transportation_create_driver/<int:id>", views.transportation_create_driver, name="transportation_create_driver"),
    path("transportation_append_passenger/<int:transport_id>/<int:registration_id>", views.transportation_append_passenger, name="transportation_append_passenger"),
    path('set_available_seats/<int:transportation_id>/<int:seats>/', views.set_available_seats, name='set_available_seats'),
    path('update_note/<int:pk>/', views.update_note, name='update_note'),
    path("transportation_public/<int:id>", views.transportation_public, name="transportation_public"),
    path("logout/", views.logout_request, name="logout"),
    path("login/", views.login_request, name="login"),
    path("hr/", views.list_members, name="list_members"),
    path("aspirants/<int:person_id>", views.list_aspirants, name="list_aspirants"),
    path("aspirants/add", views.add_aspirant, name="add_aspirant"),
    path("aspirants/edit/<int:pk>", views.edit_aspirant, name="edit_aspirant"),
    path("aspirants/accept/<int:pk>", views.accept_aspirant, name="accept_aspirant"),
    path("aspirants/edit_note/<int:pk>", views.aspirant_update_note, name="aspirant_update_note"),
    path("medical_exams/", views.medical_exams, name="medical_exams"),
    path("medical_exam/verify/<int:exam_id>/", views.verify_exam, name="verify_exam"),
    path("medical_exams_all/", views.medical_exams_all, name="medical_exams_all"),
    path("medical_exams_history/<int:person_id>", views.medical_exams_history, name="medical_exams_history"),
    path("brigade/verify/<int:brigade_id>/", views.verify_brigade, name="verify_brigade"),
    path("treasurer/<int:season_year>", views.treasurer, name="treasurer"),
    path("sinners/<int:season_year>", views.sinners, name="sinners"),
    path("brigades/<int:season_year>", views.brigades, name="brigades"),
    path("brigade_requests/", views.brigade_requests, name="brigade_requests"),
    path("brigade_history/<int:person_id>", views.brigade_history, name="brigade_history"),
    path("add_brigade/", views.add_brigade, name="add_brigade"),
    path("register/", views.register, name="register"),
    path("register_to/<int:id>", views.register_to, name="register_to"),
    path("edit_registration/<int:registration_id>", views.edit_registration, name="edit_registration"),
    path("unregister/<int:id>", views.unregister, name="unregister"),
    path("upload_new_exam_file/", views.upload_examination, name="upload_exam_file"),
    path('i18n/', include('django.conf.urls.i18n')),
    path('api/competitions', views.api_competitions, name='api_competitions'),
    path('api/registrations/<int:iscsps_id>', views.api_registrations, name='api_registrations'),
    path('api/examinations', views.api_examinations, name='api_examinations'),
    path('api/add_new_member_cus/<int:pk>', views.add_new_member_cus, name='add_new_member_cus'),
    path('api/list_new_members', views.list_new_members, name='list_new_members'),
    re_path(
        r'^payment_details/(?P<fee_type>[^/]+)/(?P<year>\d+)/(?P<amount>\d+)(?:/(?P<optional_text>[^/]+))?/$',
        views.payment_details, name='payment_details'),
    path("payment_history/<int:person_id>", views.payment_history, name='payment_history'),
    path('password-reset/', ResetPasswordView.as_view(), name='password_reset'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='main/users/password_reset_confirm.html',
                                                     success_url=reverse_lazy("main:password_reset_complete")),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='main/users/password_reset_complete.html'),
         name='password_reset_complete'),
    # path to toggle copaid for each competition
    path('toggle_co_paid/<int:competition_id>/<int:pk>/', views.toggle_co_paid, name='toggle_co_paid'),
    path('toggle_aspirant_temp_registration/<int:person_id>/', views.toggle_aspirant_temp_registration, name='toggle_aspirant_temp_registration'),
    path('toggle_aspirant_big_registration/<int:person_id>/', views.toggle_aspirant_big_registration,
         name='toggle_aspirant_big_registration'),
    path('toggle_aspirant_small_registration/<int:person_id>/', views.toggle_aspirant_small_registration,
         name='toggle_aspirant_small_registration'),
    path('toggle_aspirant_gdpr/<int:person_id>/', views.toggle_aspirant_gdpr,
         name='toggle_aspirant_gdpr'),
    path('toggle_aspirant_information_agreed/<int:person_id>/', views.toggle_aspirant_information_agreed,
         name='toggle_aspirant_information_agreed'),
    path('toggle_aspirant_pictures2pcs/<int:person_id>/', views.toggle_aspirant_pictures2pcs,
         name='toggle_aspirant_pictures2pcs'),
    path('send_payment_email/<int:person_id>/', views.send_payment_email, name='send_payment_email'),
]
