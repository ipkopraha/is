import json
import requests
from django.db.models import Q
from datetime import date
from app.settings import CZECHSWIMMING_USER, CZECHSWIMMING_PASS, CUS_PASS, CUS_USER, CSPS_DOMAIN
from django.contrib.auth.models import Group
import time
from selenium.webdriver.support.select import Select
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from django.conf import settings
from django.core.mail import send_mail
from selenium.webdriver.support.wait import WebDriverWait


def get_season_year(d: date):
    return d.year if d > date(d.year, 7, 1) else d.year - 1


def get_season_end_year(d: date):
    return d.year if d < date(d.year, 10, 1) else d.year + 1


def login_to_cus():
    from selenium import webdriver
    from selenium.webdriver.common.by import By

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--window-size=1920,1080")
    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(10)
    username = CUS_USER
    password = CUS_PASS
    driver.get('https://iscus.cz/admin/clen/novy/12081')  # head to github login pag
    WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.ID, "inputEmail"))  # This is a dummy element
    )
    time.sleep(0.1)
    driver.find_element(By.ID, "inputEmail").send_keys(username)
    driver.find_element(By.ID, "inputPassword").send_keys(password)
    driver.find_element(By.ID, "_submit").click()
    time.sleep(1)
    return driver


def login_to_csps():
    from selenium.webdriver.support.ui import WebDriverWait
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--window-size=1920,1080")
    driver = webdriver.Chrome(options=chrome_options)
    driver.implicitly_wait(10)
    username = CZECHSWIMMING_USER
    password = CZECHSWIMMING_PASS
    # head to github login page
    driver.get(f"{CSPS_DOMAIN}")

    elem = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.ID, "username"))  # This is a dummy element
    )
    # find username/email field and send the username itself to the input field
    time.sleep(0.1)
    driver.find_element("id", "username").send_keys(username)
    driver.find_element("id", "password").send_keys(password)
    driver.find_element(By.TAG_NAME, "button").click()
    time.sleep(0.3)
    return driver


def update_user_medical_examination_on_IS(first_name: str, last_name: str, id: int, date_until: str):
    from .models import InternalLog, Person
    log_entry = InternalLog(
        category="Examination Sync",
        log_entry=f"CSPS Syncing exam of {first_name} {last_name} CSPS id: {id} exam valid till {date_until}",
    )

    try:

        with requests.Session() as s:

            # LOGIN
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}
            login = s.get(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/session',
                          headers=headers)
            login_data = {
                "j_username": CZECHSWIMMING_USER,
                "j_password": CZECHSWIMMING_PASS,
            }
            site = s.post(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/j_security_check',
                          headers=headers, data=login_data, verify=False)

            # ADD EXAMINATION UNTIL
            url = f"{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/clubs/22/members/{id}/physical-inspection"
            headers = {
                "accept": "application/json",
            }

            # Assuming 'date_until' is defined and correctly formatted as 'YYYY-MM-DD'
            files = {'validTo': (None, str(date_until))}

            x = s.post(
                url,
                headers=headers,
                files=files
            )
            if x.status_code != 200:
                log_entry.result = f"Error: {x.text}"
            else:
                log_entry.result = "Success"
                return True

    except Exception as ex:
        print(ex)
        log_entry.result = ex
        # get all IT
        group = Group.objects.filter(name='IT Role').first()
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
        else:
            recipient_list = []
        subject = 'IS I.PKO - error occured during ' + log_entry.category
        message = log_entry.log_entry + "\n\n" + str(ex)
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)

    log_entry.save()


def add_new_member_to_CSPS(
        first_name: str,
        last_name: str,
        rodne_cislo: str,
        birth_date: str,
        ulice: str,
        cislo_popisne: str,
        psc: str,
        cislo_orientacni: str,
        mesto: str,
        sex: str,
        driver=None):
    """
    This function is used to test adding new member to Czech Swimming
    :param first_name: str
    :param last_name: str
    :param rodne_cislo: str
    :param birth_date: str (format "2000-12-31")
    :param ulice: str
    :param cislo_popisne: str
    :param psc: str
    :param mesto: str
    :param cislo_orientacni: str
    :param sex: str [MALE, FEMALE]
    :param driver: webdriver.Chrome
    """
    from .models import InternalLog, Person
    log_entry = InternalLog(
        category="CSPS sync",
        log_entry=f"Add new CSPS member {first_name} {last_name} {birth_date} {rodne_cislo} {ulice} {cislo_popisne} {psc} {cislo_orientacni} {mesto} {sex}",
    )
    number_int = None
    try:

        url = f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/users'
        payload = {"citizenship": "CZE", "userSportRefereeDtos": [], "userSportTrainerQualificationAndLicenceDtos": [],
                   "sportIds": [2, 3], "personalIdentificationNumber": f"{rodne_cislo.replace('/', '')}",
                   "existing": False, "userId": "",
                   "birthDate": f"{birth_date}", "sex": f"{sex}", "firstName": f"{first_name}",
                   "lastName": f"{last_name}", "street": f"{ulice}",
                   "landRegistryNumber": f"{cislo_popisne}", "houseNumber": f"{cislo_orientacni}", "zipCode": f"{psc}",
                   "city": f"{mesto}",
                   "personalInformationAcceptance": True}

        with requests.Session() as s:

            # LOGIN
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}
            login = s.get(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/session',
                          headers=headers)
            login_data = {
                "j_username": CZECHSWIMMING_USER,
                "j_password": CZECHSWIMMING_PASS,
            }
            site = s.post(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/j_security_check',
                          headers=headers, data=login_data, verify=False)

            # CREATE USER AND RETREIEVE ID
            print('PAYLOAD', payload)
            files = {
                'user': ('blob', json.dumps(payload), 'application/json',
                         {'Content-Disposition': 'form-data; name="user"; filename="blob"'})
            }
            x = s.post(url, files=files)
            print(x.status_code)
            print(x.text)

            if x.status_code == 200:

                number = x.text
                number_int = int(number)

                # ADD USER TO CLUB
                site = s.post(
                    f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/clubs/22/members/{number_int}?sportIds=3&sportIds=2',
                    headers={'Content-Type': 'application/json'}
                )
                log_entry.result = f"\n\n{x.status_code} : Success?"
            elif x.status_code == 400:
                # user likely already exists, check it
                x = s.get(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/users/{rodne_cislo.replace("/", "")}')

                response_data = x.json()
                user_id = response_data.get("userId")
                number_int = int(user_id)

                # ADD USER TO CLUB
                site = s.post(
                    f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/clubs/22/members/{number_int}?sportIds=3&sportIds=2',
                    headers={'Content-Type': 'application/json'}
                )
                if x.status_code == 200:
                    log_entry.result = f"\n\n{x.status_code} : Success"
                else:
                    log_entry.result = f"\n\n{x.status_code} : Failed -> {x.text}"
                    number_int = None

            # append x.text to log
            log_entry.result += f"\n\n{x.text}"


    except Exception as ex:
        print(ex)
        log_entry.result = ex
        # get all IT
        group = Group.objects.filter(name='IT Role').first()
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
        else:
            recipient_list = []
        subject = 'IS I.PKO - error occured during ' + log_entry.category
        message = log_entry.log_entry + "\n\n" + str(ex)
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)
    log_entry.save()
    return number_int


def add_new_member_to_CUS(
        first_name: str,
        last_name: str,
        rodne_cislo: str,
        ulice: str,
        cislo_popisne: str,
        cislo_orientacni: str,
        mesto: str,
        psc: str,
        phone: str,
        email: str,
        driver=None):
    """
    """

    import time
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions import NoSuchElementException

    from .models import InternalLog, Person
    # if CSPS_DOMAIN contains 'dev' leave this function

    if 'dev' in CSPS_DOMAIN:
        return

    log_entry = InternalLog(
        category="CUS sync",
        log_entry=f"Add new CUS member: {first_name} {last_name} {rodne_cislo} {ulice} {cislo_popisne} {cislo_orientacni} {mesto} {psc} {phone} {email}",
    )
    try:
        if driver is None:
            driver = login_to_cus()

        time.sleep(1.)
        try:
            x_path = '//*[@id="termsfeed-com---nb"]/div/div[3]/button[1]'
            driver.find_element(By.XPATH, x_path).click() # GDPR consent
        except NoSuchElementException as e:
            print('No GDPR consent')
        # First Name
        driver.find_element(By.ID, 'member_firstName').send_keys(f'{first_name}')
        # Last Name
        driver.find_element(By.ID, 'member_lastName').send_keys(f'{last_name}')
        # Rodne cislo
        driver.find_element(By.ID, 'member_birthNumber').send_keys(f'{rodne_cislo}')
        # noAutocompleteStreet
        driver.find_element(By.ID, 'member_street').send_keys(f'{ulice}')
        # noAutocompleteLandRegistryNumber
        driver.find_element(By.ID, 'member_houseNumber').send_keys(f'{cislo_popisne}')
        # noAutocompleteZipCode
        driver.find_element(By.ID, 'member_zip').send_keys(f'{psc}')
        # noAutocompleteHouseNumber
        driver.find_element(By.ID, 'member_orientationNumber').send_keys(f'{cislo_orientacni}')
        # noAutocompleteCity
        driver.find_element(By.ID, 'member_city').send_keys(f'{mesto}')
        # noAutocompleteHouseNumber
        driver.find_element(By.ID, 'member_phone').send_keys(f'{phone}')
        # noAutocompleteCity
        driver.find_element(By.ID, 'member_email').send_keys(f'{email}')
        driver.find_element(By.XPATH, '//*[@id="member_role_chosen"]/a').click()
        time.sleep(1)
        driver.find_element(By.XPATH, '//*[@id="member_role_chosen"]/div/ul/li[2]').click()
        time.sleep(1)
        today = date.today()
        todays_date = today.strftime("%d.%m.%Y")
        driver.find_element(By.ID, 'member_athleteFrom').send_keys(f'{todays_date}')
        time.sleep(1)

        driver.find_element(By.ID, "member_save").click()
        time.sleep(5)
    except Exception as ex:
        print(ex)
        log_entry.result = ex
        # get all IT
        group = Group.objects.filter(name='IT Role').first()
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
        else:
            recipient_list = []
        subject = 'IS I.PKO - error occured during ' + log_entry.category
        message = log_entry.log_entry + "\n\n" + str(ex)
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)

    log_entry.result = "Success"
    log_entry.save()


def get_unfulfilled_lists(season_year):
    from .models import FulfilledMembership, MemberStatus, Person, FulfilledBrigade

    # Update or create FulfilledMembership for missing people
    mems = FulfilledMembership.objects.filter(
        Q(year=season_year),
        Q(person__user__is_active=True),
        Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping)
    )
    people_in_mem = [a.person for a in mems]
    missing_people = set(Person.objects.filter(
        Q(user__is_active=True),
        Q(status=MemberStatus.Member) | Q(status=MemberStatus.Escaping)
    )).difference(people_in_mem)
    for missing_person in missing_people:
        FulfilledMembership.objects.update_or_create(
            year=season_year,
            person=missing_person,
            defaults={
                'fulfilled_membership': False,
                'missing_membership_money': 0
            }
        )

    # Update or create FulfilledBrigade for missing people
    mems = FulfilledBrigade.objects.filter(
        Q(year=season_year),
        Q(person__user__is_active=True),
        Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping)
    )
    people_in_mem = [a.person for a in mems]
    missing_people = set(Person.objects.filter(
        Q(status=MemberStatus.Member) | Q(status=MemberStatus.Escaping),
        user__is_active=True
    )).difference(people_in_mem)
    for missing_person in missing_people:
        FulfilledBrigade.objects.update_or_create(
            person=missing_person,
            year=season_year,
            defaults={
                'fulfilled_brigade': False,
                'missing_brigade_hours': 0,
                'missing_brigade_money': 0
            }
        )

    # Get fulfilled memberships and brigades
    fulfilled_memberships_w = FulfilledMembership.objects \
        .filter(
            Q(year=season_year),
            Q(fulfilled_membership=False),
            Q(person__user__is_active=True),
            Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping)
        ) \
        .select_related('person') \
        .order_by('person__last_name', 'person__first_name', 'person__birth_date')

    fulfilled_brigade_w = FulfilledBrigade.objects \
        .filter(
            Q(year=season_year - 1),
            Q(fulfilled_brigade=False),
            Q(person__user__is_active=True),
            Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping)
        ) \
        .select_related('person') \
        .order_by('person__last_name', 'person__first_name', 'person__birth_date')

    brigade_set = set([a[0] for a in fulfilled_brigade_w.values_list('person')])
    membership_set = set([a[0] for a in fulfilled_memberships_w.values_list('person')])
    sinners_set = brigade_set.union(membership_set)
    fulfilled_brigade = FulfilledBrigade.objects \
        .filter(year=season_year - 1, person__in=sinners_set) \
        .select_related('person') \
        .order_by('person__last_name', 'person__first_name', 'person__birth_date')
    fulfilled_memberships = FulfilledMembership.objects \
        .filter(year=season_year, person__in=sinners_set) \
        .select_related('person') \
        .order_by('person__last_name', 'person__first_name', 'person__birth_date')

    return fulfilled_brigade, fulfilled_memberships


def remove_atendee_from_transportation(competion_id, person_id):
    from .models import Transport
    # find all transports where this person is drive and delete them
    transports = Transport.objects.filter(competition_id=competion_id, driver_id=person_id)
    for transport in transports:
        print(transport)
        transport.delete()
    # find all transports where this person is passenger remove the person from passengers
    transports = Transport.objects.filter(competition_id=competion_id, passengers=person_id)
    for transport in transports:
        print(transport)
        transport.passengers.remove(person_id)
        transport.save()
    return True


def apply_for_competition(competition_id, person_id, length, championship_course=False):
    from .models import InternalLog

    with requests.Session() as s:
        category_id, csps_id, applicant = get_competition_category(s, person_id, competition_id, length, championship_course)
        if category_id is not None and csps_id is not None:
            url = f"{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/competitions/{competition_id}/applications/{category_id}?userId={csps_id}"
            headers = {
                "accept": "application/json",
            }
            x = s.post(url, headers=headers)
            print(x.text)
            if x.status_code == 200:
                print(f"Apply { applicant } { csps_id } to competition { competition_id }  category {category_id} success")
                return True
            else:
                log_entry = InternalLog(
                    category="Competition application",
                    log_entry=f"Apply { applicant } { csps_id} to competition { competition_id }  category {category_id} failed with {x.text}",
                    result="Failed"
                )
                log_entry.save()
                print(f"Apply { applicant } {csps_id} to competition {competition_id}  category {category_id} failed with {x.text}")
                return False

        log_entry = InternalLog(
                    category="Competition application",
                    log_entry=f"No category found for {applicant} in competition {competition_id} and for length {length}",
                    result="Undefined"
                )
        log_entry.save()
        return False


def delete_competition_application(competition_id, person_id, length, championship_course=False):
    from .models import InternalLog

    with requests.Session() as s:
        category_id, csps_id, applicant = get_competition_category(s, person_id, competition_id, length, championship_course)
        if category_id is not None and csps_id is not None:
            url = f"{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/competitions/{competition_id}/applications/{category_id}?userId={csps_id}"
            headers = {
                "accept": "application/json",
            }
            x = s.delete(url, headers=headers)
            print(x.text)
            if x.status_code == 200:
                print(f"Delete { applicant } { csps_id } from competition { competition_id }  category {category_id} success")
                return True
            else:
                log_entry = InternalLog(
                    category="Delete application",
                    log_entry=f"Delete { applicant } { csps_id} from competition { competition_id }  category {category_id} failed with {x.text}",
                    result="Failed"
                )
                log_entry.save()
                print(f"Delete { applicant } {csps_id} from competition {competition_id}  category {category_id} failed with {x.text}")
                return False


        log_entry = InternalLog(
                    category="Application delete",
                    log_entry=f"No category found for {applicant} in competition {competition_id} and for length {length}",
                    result="Undefined"
                )
        log_entry.save()
        return False


def get_competition_category(s, person_id, competition_id, length, championship_course=False):
    from .models import Person
    import re

    leng = int(re.search(r'\d+', length).group())
    # LOGIN
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}
    login = s.get(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/session',
                  headers=headers)
    login_data = {
        "j_username": CZECHSWIMMING_USER,
        "j_password": CZECHSWIMMING_PASS,
    }
    site = s.post(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/j_security_check',
                  headers=headers, data=login_data, verify=False)

    # download competition details
    url = f"{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/competitions/{competition_id}"
    headers = {
        "accept": "application/json",
    }
    x = s.get(url, headers=headers)
    data = x.json()
    # Extract the required information
    categories = []
    for half_day in data.get('halfDayDtos', []):
        for category in half_day.get('categoryDtos', []):
            discipline_abbrev = category['disciplineAbbrev']
            # Extract the numeric part of the disciplineAbbrev
            discipline_number = int(re.search(r'\d+', discipline_abbrev).group())
            championship_category = category['note'] == "MČR"
            categories.append({
                'categoryId': category['competitionCategoryId'],
                'gender': category['gender'],
                'disciplineAbbrev': discipline_number,
                'bornFrom': category['bornFrom'],
                'bornTo': category['bornTo'],
                'championshipCategory': championship_category
            })


    # select the first category that maches the length, gender and age
    person = Person.objects.get(pk=person_id)
    born = person.birth_date.year
    selected_category = None
    for category in categories:
        if category['disciplineAbbrev'] == leng and category['gender'].lower() == person.sex.lower() and category[
            'bornFrom'] <= born <= category['bornTo'] and category['championshipCategory'] == championship_course:
            selected_category = category
            break
    if selected_category is not None:
        return selected_category['categoryId'], person.iscsps_id, str(person)
    else:
        return None, None, str(person)


