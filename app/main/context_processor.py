from .models import Person


def current_person(request):
    """
    Adds Person to context
    """
    context = {'person': None}
    if request.user.is_authenticated:
        try:
            p = Person.objects.get(user=request.user)
        except Person.DoesNotExist:
            p = None

        context = {'person': p}

    return context
