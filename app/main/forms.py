import datetime

from django import forms
from django.contrib.auth.models import Group, User
from django.db.models import QuerySet, Q
from django.utils.translation import gettext_lazy as _

from .models import Registration, Person, Competition, MedicalExamination, Brigade, Payment, MemberStatus

transports = (
    ("", _('---------')),  # default choice
    (0, _('Individual')),
    (1, _('I need a ride')),
    (2, _('I go by car and I have 1 spare place')),
    (3, _('I go by car and I have 2 spare places')),
    (4, _('I go by car and I have 3 spare places')),
    (5, _('I go by car and I have 4 spare places')),
    (6, _('I go by car and I have 5 spare places')),
    (7, _('I go by car and I have 6 spare places')),
)


class RegistrationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.competition = kwargs.pop('competition', None)
        self.person = kwargs.pop('person', None)
        length_queryset = kwargs.pop('lengths', None)
        cs_length = kwargs.pop('cs_length', None)
        track_editable = kwargs.pop('track_editable', False)
        if track_editable is False and 'instance' in kwargs:
            self.disabled_length_choices = kwargs['instance'].length.all()
            self.cs_course = kwargs['instance'].championship_course
        else:
            self.disabled_length_choices = None
            self.cs_course = None

        super(RegistrationForm, self).__init__(*args, **kwargs)

        self.fields['person'] = forms.ModelChoiceField(
            queryset=Person.objects.filter(Q(pk=self.person.pk) | Q(person_allowed_to_register=self.person.pk)),
            initial=self.person, label=_("Person"), )
        self.fields['competition'] = forms.ModelChoiceField(queryset=Competition.objects.filter(pk=self.competition.pk),
                                                            initial=self.competition,
                                                            label=_("Competition"),
                                                            disabled=True)

        if self.competition.is_winter_competition:
            if self.competition.is_championship_venue:
                self.fields['championship_course'] = forms.BooleanField(label=_('I will swim the championship course'),
                                                                        required=False,
                                                                        disabled=not track_editable)
            else:
                del self.fields['championship_course']

            self.fields['length'] = forms.ModelMultipleChoiceField(
                label=_("Lengths"),
                queryset=length_queryset,
                disabled=not track_editable
            )
        else:
            del self.fields['championship_course']
            self.fields['length'] = forms.ModelMultipleChoiceField(
                queryset=length_queryset,
                label=_("Lengths"),
                disabled=not track_editable
            )

        if self.competition.event_venue.is_homeplace:
            self.fields['transport'] = forms.ChoiceField(label=_("Transport"), choices=(transports[1],))
        else:
            self.fields['transport'] = forms.ChoiceField(label=_("Transport"), choices=transports, required=True)
            drivers = Registration.objects.filter(competition=self.competition, transport__gt=1).values_list('person')
            self.fields['preferred_driver'] = forms.ModelChoiceField(
                queryset=Person.objects.filter(pk__in=drivers).exclude(pk=self.person.pk),
                label=_(
                    "Choose your preferred driver. The final decision depends on the demands and availability and is made by the transport manager."),
                required=False,
            )
            self.fields['preferred_driver'].label_from_instance = lambda obj: obj.get_public_name()

            available_passengers = Registration.objects.filter(competition=self.competition, transport=1).values_list(
                'person')

            self.fields['preferred_passengers'] = forms.ModelMultipleChoiceField(
                queryset=Person.objects.filter(pk__in=available_passengers).exclude(pk=self.person.pk),
                label=_(
                    "Choose your preferred passengers. The final decision depends on the demands and availability and is made by the transport manager."),
                required=False,
            )
            self.fields['preferred_passengers'].label_from_instance = lambda obj: obj.get_public_name()

            del self.fields['note']  # this is due to ordering
            self.fields['note'] = forms.CharField(label=_(
                "Note (i.e. I drive only one way, Who is going with me, Vegetarian food request etc.)"),
                required=False)

    class Meta:
        model = Registration
        fields = ('person', 'competition', 'championship_course', 'length', 'transport', 'note', 'preferred_driver', 'preferred_passengers')

    def clean_length(self):
        data = self.cleaned_data['length']
        is_winter = self.competition.is_winter_competition
        if is_winter and len(data) > 1:
            raise forms.ValidationError(_("You can select only one length."))

        if self.disabled_length_choices:
            return self.disabled_length_choices
        print ("Application of ", self.person, " selected lengths ", data)
        if len(data) == 0:
            raise forms.ValidationError(_("You must select at least one length."))
        return data

    def clean_championship_course(self):
        data = self.cleaned_data['championship_course']
        if self.cs_course:
            return self.cs_course
        return data


class ExaminationForm(forms.ModelForm):
    class Meta:
        model = MedicalExamination
        fields = ('examination_file_link', 'examination_until')


class BrigadeForm(forms.ModelForm):
    class Meta:
        model = Brigade
        fields = ('date', 'work', 'hours', 'accountable')

    def __init__(self, *args, **kwargs):
        super(BrigadeForm, self).__init__(*args, **kwargs)
        group = Group.objects.get(name='Brigade Manager')
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = Person.objects.filter(user__in=all_recipients).order_by('last_name')
            self.fields['accountable'] = forms.ModelChoiceField(queryset=recipient_list, label=_("Accountable"))


class MedicalExaminationRejectionForm(forms.Form):
    note = forms.CharField(widget=forms.TextInput(attrs={'size': 10, 'placeholder': _("Reason for rejection:")}),
                           label=_('Reason'),
                           required=True)


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ('membership_amount', 'brigade_amount', 'person', 'way_of_transfer')
        widgets = {'person': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.fields['membership_amount'] = forms.CharField(widget=forms.TextInput(attrs={'size': 2, 'placeholder': 0}),
                                                           label=_('MF'), required=False)
        self.fields['brigade_amount'] = forms.CharField(widget=forms.TextInput(attrs={'size': 2, 'placeholder': 0}),
                                                        label=_('BR'), required=False)
        self.fields['person'].label = ''

    def clean_membership_amount(self):
        data = self.cleaned_data['membership_amount']
        if data == '':
            return 0
        else:
            return data

    def clean_brigade_amount(self):
        data = self.cleaned_data['brigade_amount']
        if data == '':
            return 0
        else:
            return data


class AspirantForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'first_name',
            'last_name',
            'email',
            'is_czech_citizenship',
            'RC',
            'birth_date',
            'sex',
            'phone_number',
            'temp_registration',
            'small_reg_form',
            'big_reg_form',
            'gdpr_agreed',
            'information_agreed',
            'pictures2pcs',
            'hosted_member',
            'adr_street',
            'adr_home_number_popisne',
            'adr_home_number2_orientacni',
            'adr_post_code',
            'adr_city',
            'aspirant_note',
        ]
        exclude = ['user', 'member_since', 'is_honorable', 'pass_id', 'points_id',
                   'iscsps_id', 'person_allowed_to_register', 'level_now', 'level_last_year',
                   'history', 'status', 'address']

    def __init__(self, *args, **kwargs):
        self.as_manager = kwargs.pop('as_manager', None)
        super(AspirantForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        if self.instance and self.instance.pk:
            self.fields['email'].disabled = True
        group = Group.objects.get(name='HR role')
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = Person.objects.filter(user__in=all_recipients).order_by('last_name')
            self.fields['aspirant_manager'] = forms.ModelChoiceField(queryset=recipient_list)
        self.fields['aspirant_manager'].initial = self.as_manager  # Setting aspirant manager to current user

    def save(self, commit=True):
        person = super(AspirantForm, self).save(commit=False)
        email = person.email
        user, created = User.objects.get_or_create(username=email, defaults={'email': email, 'password': self.generate_random_password()})
        if not created:
            user.email = email
            user.set_password(self.generate_random_password())
        user.is_staff = True
        user.save()
        person.user = user
        if created:
            person.member_since = datetime.date.today()  # Setting member_since to current date
        person.status = MemberStatus.Aspirant
        person.aspirant_manager = self.cleaned_data['aspirant_manager']
        if commit:
            person.save()
        return person

    def generate_random_password(self):
        import random
        import string
        length = 10
        characters = string.ascii_letters + string.digits + string.punctuation
        return ''.join(random.choice(characters) for i in range(length))
