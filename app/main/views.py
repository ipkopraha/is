import csv
import json
import re
import ssl
import urllib
import io
from datetime import date, datetime, timedelta
from urllib.error import URLError
from urllib.request import urlopen

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import PasswordResetView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.db.models import Max, Q, ExpressionWrapper, F, fields
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.http import require_POST

from .utils import update_user_medical_examination_on_IS, add_new_member_to_CSPS, add_new_member_to_CUS, \
    get_unfulfilled_lists, remove_atendee_from_transportation, apply_for_competition, delete_competition_application
from .forms import RegistrationForm, ExaminationForm, BrigadeForm, PaymentForm, MedicalExaminationRejectionForm, \
    AspirantForm
from .models import Competition, Person, MedicalExamination, Brigade, Payment, Registration, FulfilledMembership, \
    FulfilledBrigade, ChampionshipCourseRule, MemberStatus, Transport
from .utils import get_season_year


def is_treasurer(user):
    return user.groups.filter(name='Treasurer Role').exists()


def is_hr(user):
    return user.groups.filter(name='HR role').exists()


def is_competition_manager(user):
    return user.groups.filter(name='Competition Manager Role').exists()


def is_medic(user):
    return user.groups.filter(name='Medical Examination Role').exists()


def is_brigade_role(user):
    return user.groups.filter(name='Brigade Manager').exists()


def homepage(request):
    return render(request=request,
                  template_name="main/home.html",
                  context={"competitions": Competition.objects.all})


transports = (
    ("", _('---------')),  # default choice
    (0, _('Individual')),
    (1, _('I need a ride')),
    (2, _('I drive (1 spare pl.)')),
    (3, _('I drive (2 spare pl.)')),
    (4, _('I drive (3 spare pl.)')),
    (5, _('I drive (4 spare pl.)')),
    (6, _('I drive (5 spare pl.)')),
    (7, _('I drive (6 spare pl.)')),
)


@login_required
def update_membership_payments(person: Person, current_date: date, request=None):
    """
    BRIGADE:
    Given year, check payments between 1.7.YEAR-1 to 1.7.YEAR
    """
    person.update_fees(current_date, request)


@login_required
@user_passes_test(is_treasurer)
def treasurer(request, season_year):
    if request.method == 'POST':
        form = PaymentForm(request.POST)
        payment = form.save(commit=False)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = PaymentForm()

    # check, if FulfilledMembership has missing some people for season_year. If so, recreate them
    # Check Memberships
    mems = FulfilledMembership.objects.filter(year=season_year, person__user__is_active=True)
    people_in_mem = [a.person for a in mems]
    missing_people = set(Person.objects.filter(user__is_active=True)).difference(people_in_mem)
    for missing_person in missing_people:
        FulfilledMembership.objects.update_or_create(year=season_year,
                                                     person=missing_person,
                                                     defaults={
                                                         'fulfilled_membership': False,
                                                         'missing_membership_money': 0
                                                     })

    # Check Brigades
    mems = FulfilledBrigade.objects.filter(year=season_year, person__user__is_active=True)
    people_in_mem = [a.person for a in mems]
    missing_people = set(Person.objects.filter(user__is_active=True)).difference(people_in_mem)
    for missing_person in missing_people:
        FulfilledBrigade.objects.update_or_create(person=missing_person,
                                                  year=season_year,
                                                  defaults={
                                                      'fulfilled_brigade': False,
                                                      'missing_brigade_hours': 0,
                                                      'missing_brigade_money': 0})

    fulfilled_memberships = FulfilledMembership.objects \
        .filter(year=season_year, person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')
    fulfilled_brigade = FulfilledBrigade.objects \
        .filter(year=season_year, person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')
    fulfilled_brigade_prev = FulfilledBrigade.objects \
        .filter(year=season_year - 1, person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')

    years = FulfilledMembership.objects.order_by('-year').values_list('year').distinct()
    years = [a[0] for a in years]

    if len(fulfilled_memberships) != len(fulfilled_brigade):
        # find missing:
        brigade_set = set([a[0] for a in fulfilled_brigade.values_list('person')])
        membership_set = set([a[0] for a in fulfilled_memberships.values_list('person')])

        for missing_person in list((membership_set - brigade_set).union(brigade_set - membership_set)):
            # print(missing_person)
            Person.objects.get(pk=missing_person).update_fees(date(season_year, 10, 1), request)

    return render(request,
                  'main/treasurer.html',
                  {"data": zip(fulfilled_memberships, fulfilled_brigade, fulfilled_brigade_prev),
                   'season_year': season_year,
                   'years': years,
                   'form': form})


@login_required
@user_passes_test(is_treasurer)
def sinners(request, season_year):
    if request.method == 'POST':
        form = PaymentForm(request.POST)
        payment = form.save(commit=False)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
    else:
        form = PaymentForm()

    years = FulfilledMembership.objects.order_by('-year').values_list('year').distinct()
    years = [a[0] for a in years]

    fulfilled_brigade, fulfilled_memberships = get_unfulfilled_lists(season_year)

    return render(request,
                  'main/sinners.html',
                  {"data": zip(fulfilled_memberships, fulfilled_brigade),
                   'season_year': season_year,
                   'years': years,
                   'form': form})


@login_required
@user_passes_test(is_brigade_role)
def brigades(request, season_year=None):
    if season_year is None:
        season_year = get_season_year(date.today())
    fulfilled_brigade = (FulfilledBrigade.objects.filter(Q(year=season_year),
                                                         Q(person__user__is_active=True),
                                                         Q(person__status=MemberStatus.Member) | Q(
                                                             person__status=MemberStatus.Escaping)).select_related(
        'person')
                         .order_by('person__last_name', 'person__first_name'))

    years = FulfilledBrigade.objects.order_by('-year').values_list('year').distinct()
    years = [a[0] for a in years]

    return render(request,
                  'main/brigades.html',
                  {'data': fulfilled_brigade,
                   'season_year': season_year,
                   'years': years})


@login_required
def user_info(request):
    person = get_current_person(request)
    if person.is_aspirant:
        return redirect("main:aspirant_info")
    try:
        last_examination = MedicalExamination.objects.filter(person=person, rejected=False).latest('examination_until')
    except MedicalExamination.DoesNotExist:
        last_examination = None
    fulfilled_membership_this_year = person.did_user_pay_membership(year=date.today().year)
    fulfilled_membership_prev_year = person.did_user_pay_membership(year=date.today().year - 1)

    season_year = get_season_year(date.today())
    fulfilled_brigade_this_year = person.did_user_pay_brigade(season_year=season_year)
    fulfilled_brigade_prev_year = person.did_user_pay_brigade(season_year=season_year - 1)
    fulfilled_brigade_prev2_year = person.did_user_pay_brigade(season_year=season_year - 2)

    brigades_to_verify = Brigade.objects.filter(person=person,
                                                verified=False)
    brigade_hours_to_verify = sum([b.hours for b in brigades_to_verify])

    my_registrations = Registration.objects.filter(person=person,
                                                   competition__signing_from__lte=date.today(),
                                                   competition__event_date__gte=date.today())
    registration_removable = [date.today() >= r.competition.signing_from and date.today() <= r.competition.signing_until
                              for r in my_registrations]

    return render(request=request,
                  template_name="main/user_info.html",
                  context={"person": person,
                           "season_year": season_year,
                           "current_year": date.today().year,
                           "examination": last_examination,
                           "fulfilled_membership_this_year": fulfilled_membership_this_year,
                           "fulfilled_membership_prev_year": fulfilled_membership_prev_year,
                           "fulfilled_brigade_this_year": fulfilled_brigade_this_year,
                           "fulfilled_brigade_prev_year": fulfilled_brigade_prev_year,
                           "fulfilled_brigade_prev2_year": fulfilled_brigade_prev2_year,
                           "brigade_hours_to_verify": brigade_hours_to_verify,
                           "my_registrations": zip(my_registrations, registration_removable),
                           "transports": transports,
                           })


@login_required
def aspirant_info(request):
    person = get_current_person(request)
    if not person.is_aspirant:
        return redirect("main:user_info")

    try:
        last_examination = MedicalExamination.objects.filter(person=person).latest('examination_until')
    except MedicalExamination.DoesNotExist:
        last_examination = None
    fulfilled_membership_this_year = person.did_user_pay_membership(year=date.today().year)

    season_year = get_season_year(date.today())

    return render(request=request,
                  template_name="main/aspirant_info.html",
                  context={"person": person,
                           "season_year": season_year,
                           "current_year": date.today().year,
                           "examination": last_examination,
                           "fulfilled_membership_this_year": fulfilled_membership_this_year,
                           })


@login_required
def get_current_person(request):
    try:
        person = Person.objects.get(user=request.user)
        return person
    except Person.DoesNotExist:
        return render(request=request,
                      template_name="main/error.html",
                      context={
                          "message": _(
                              "Your username does not have assigned any Person object. Please, contact administrator.")})


@login_required
def upload_examination(request):
    if request.method == 'POST':
        form = ExaminationForm(request.POST, request.FILES)
        exam = form.save(commit=False)
        exam.person = get_current_person(request)
        if form.is_valid():
            form.save()
            messages.info(request,
                          _("Medical examination uploaded. Please wait for acceptation - you will receive the result by email."))
            return redirect("main:user_info")
    else:
        form = ExaminationForm()

    return render(request=request,
                  template_name="main/upload_exam.html",
                  context={'form': form})


@login_required
def logout_request(request):
    logout(request)
    messages.info(request, _("Logged out successfully."))
    return redirect("main:homepage")


def sync_swimming_levels(request):
    current_year = date.today().year
    try:
        person = get_current_person(request)
        if person.is_aspirant:
            pass
        elif person.points_id == 0:
            sync_persons_with_points(request)
        else:
            try:
                page = urllib.request.urlopen(
                    f'https://bodovani.zimni-plavani.info/plavec.php?rok={current_year}&id={person.points_id}',
                    context=ssl._create_unverified_context())
            except:
                try:
                    page = urllib.request.urlopen(
                        f'https://81.91.85.15/plavec.php?rok={current_year}&id={person.points_id}',
                        context=ssl._create_unverified_context())
                except:
                    return
            content = page.read().decode("utf-8")
            rest = content.split('v této sezóně:')[1]
            current_value = rest.split('>')[1].split('<')[0]
            rest = rest.split('v předchozí sezóně: ')[1]
            last_value = rest.split('>')[1].split('<')[0]
            current_key = Person.Levels._member_names_.index(
                current_value) if current_value in Person.Levels._member_names_ else Person.Levels.O
            last_key = Person.Levels._member_names_.index(
                last_value) if last_value in Person.Levels._member_names_ else Person.Levels.O
            person.level_now = current_key
            person.level_last_year = last_key
            person.save()

    except Person.DoesNotExist:
        pass


def sync_persons_with_points(request):
    current_year = date.today().year
    try:
        page = urllib.request.urlopen(
            f'https://bodovani.zimni-plavani.info/plavci.php?rok={current_year}&filter%5Bxoddil%5D=0&filter%5Boddil%5D%5B0%5D=1005&sort=c',
            context=ssl._create_unverified_context())
    except URLError as e:
        try:
            page = urllib.request.urlopen(
                f'https://81.91.85.15/plavci.php?rok={current_year}&filter%5Bxoddil%5D=0&filter%5Boddil%5D%5B0%5D=1005&sort=c',
                context=ssl._create_unverified_context())

        except:
            messages.warning(request, "Could not sync swimming level.")
            return

    content = page.read().decode("utf-8")
    trs = content.split('<tr onmouseover')
    for tr in trs[1:]:
        # get persons ID
        points_id = int(tr.split('<td')[2].split('id=')[1].split('"')[0])

        # get person name
        full_name_string = tr.split('<td')[2].split('>')[2].split('<')[0]
        last_name = full_name_string[:full_name_string.find(' ')]
        after_last_name = full_name_string[full_name_string.find(' ') + 1:]
        if ',' in after_last_name:
            after_last_name = after_last_name.split(',')[0]

        if ')' in after_last_name:
            first_names = after_last_name.split(')')[1].strip()
        else:
            first_names = after_last_name.strip()

        # person year
        person_year = int(re.findall(r'\d+', tr.split('<td')[3])[0])

        # person_current_level
        current_level = tr.split('<td')[5].split('>')[1].split('<')[0]
        if current_level == '&nbsp;':
            current_level = 'O'

        # person_prev_level
        last_level = tr.split('<td')[6].split('>')[1].split('<')[0]
        if last_level == '&nbsp;':
            last_level = 'O'

        # print(first_names, last_name, person_year, current_level, last_level)
        found_persons = Person.objects.filter(first_name=first_names,
                                              last_name=last_name,
                                              birth_date__year=person_year)
        if len(found_persons) == 0:
            pass
        else:
            found_person = found_persons.first()
            current_key = Person.Levels._member_names_.index(current_level)
            last_key = Person.Levels._member_names_.index(last_level)

            changed = False
            if found_person.level_now != current_key:
                found_person.level_now = current_key
                changed = True

            if found_person.level_last_year != last_key:
                found_person.level_last_year = last_key
                changed = True

            if found_person.points_id != points_id:
                found_person.points_id = points_id
                changed = True

            if changed:
                found_person.save()


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if date.today().month < 5 or date.today().month >= 10:
                    sync_swimming_levels(request)
                messages.info(request, _('You are now logged in as') + ' ' + str(username) + '!')
                return redirect("main:user_info")
            else:
                messages.error(request, _("Invalid username or password!"))
        else:
            messages.error(request, _("Invalid username or password!"))

    form = AuthenticationForm()
    return render(request, "main/login.html", {"form": form})


@login_required
def list_competitions(request):
    active_competitions = Competition.objects.order_by('-event_date')

    return render(request,
                  'main/competitions.html',
                  {"competitions": active_competitions})



@login_required
@user_passes_test(is_competition_manager)
def list_attendees(request, id):
    competition = Competition.objects.get(pk=id)
    transports = Transport.objects.filter(competition=competition)
    registrations = Registration.objects.filter(competition=competition).order_by('person__last_name')
    passengers = []
    for registration in registrations:
        r = [a for a in transports if a.driver == registration.person]
        if r:
            passengers.append([p.last_name for p in r[0].passengers.all()])
        else:
            passengers.append([])
    all_emails = ','.join([registration.person.user.email for registration in registrations])

    if request.GET and request.GET['format'] == 'csv':
        header = ['#', _('Last Name'), _('First name'), _('Year'), _('Phone number'), _('Level this season'),
                  _('Level previous season'), _('CSC'), _('Length'), _('Transportation'), _('Co Paid'), _('Note')]

        output = io.StringIO()
        writer = csv.writer(output, dialect=csv.excel, delimiter=";")
        writer.writerow(header)
        for i, registration in enumerate(registrations):
            # if length is integer, convert it to int, otherwise leave it as float
            lengths = [str(int(a.length)) if float(a.length) == int(float(a.length)) else str(float(a.length)) for a in
                       registration.length.all()]
            championship_course = _("CSC") if registration.championship_course else ""
            writer.writerow([
                i + 1,
                registration.person.last_name,
                registration.person.first_name,
                registration.person.get_birth_year,
                registration.person.phone_number,
                registration.person.get_level_now_pretty,
                registration.person.get_level_last_year_pretty,
                championship_course,
                ','.join(lengths),
                registration.get_transport_pretty,
                "Ano" if registration.co_paid else "Ne",
                registration.note,
            ])

        response = HttpResponse(output.getvalue())
        response['Content-Disposition'] = f'attachment; filename="{str(competition)}.csv"'
        response['Content-Type'] = 'text/csv'
        return response

    if request.GET and request.GET['format'] == 'csw':
        header = ['#', _('Last Name'), _('First name'), _('Year'), _('Phone number'), _('Level this season'),
                  _('Level previous season'), _('CSC'), _('Length'), _('Transportation'), _('Co Paid'), _('Note')]

        buffer = io.StringIO()

        writer = csv.writer(buffer, dialect=csv.excel, delimiter=";")
        writer.writerow(header)
        for i, registration in enumerate(registrations):
            # if length is integer, convert it to int, otherwise leave it as float
            lengths = [str(int(a.length)) if float(a.length) == int(float(a.length)) else str(float(a.length)) for a in
                       registration.length.all()]
            championship_course = _("CSC") if registration.championship_course else ""
            writer.writerow([
                i + 1,
                registration.person.last_name,
                registration.person.first_name,
                registration.person.get_birth_year,
                registration.person.phone_number,
                registration.person.get_level_now_pretty,
                registration.person.get_level_last_year_pretty,
                championship_course,
                ','.join(lengths),
                registration.get_transport_pretty,
                "Ano" if registration.co_paid else "Ne",
                registration.note,
            ])

        response = HttpResponse(buffer.getvalue(), content_type='text/csv; charset=windows-1250')
        response['Content-Disposition'] = f'attachment; filename="{str(competition)}.csv"'

        return response

    return render(request,
                  'main/competition_attendees.html',
                  {"competition": competition,
                   "registrations": zip(registrations, passengers),
                   "all_emails": all_emails})


@login_required
def list_attendees_public(request, id):
    competition = Competition.objects.get(pk=id)
    transportations = Transport.objects.filter(competition=competition)
    registrations = Registration.objects.filter(competition=competition).order_by('person__last_name')
    passengers = []
    for registration in registrations:
        r = [a for a in transportations if a.driver == registration.person]
        if r:
            passengers.append([str(p) for p in r[0].passengers.all()])
        else:
            passengers.append([])

    return render(request,
                  'main/competition_attendees_public.html',
                  {"competition": competition,
                   "registrations": zip(registrations, passengers),
                   "transports": transports,
                   })

@login_required
@user_passes_test(is_competition_manager)
def edit_transportation_admin(request, id):
    competition = Competition.objects.get(pk=id)
    registrations = Registration.objects.filter(competition=id)
    transportations = Transport.objects.filter(competition=id)
    drivers = transportations.values_list('driver', flat=True)
    passengers = transportations.values_list('passengers', flat=True)
    # registrations = sorted(registrations, key=lambda x: x.person.id not in drivers and x.person.id not in passengers and x.transport != 0, reverse=True)
    registrations = sorted(registrations, key=lambda x: (not (x.person.id not in drivers and x.person.id not in passengers and x.transport != 0), x.person.last_name, x.person.first_name))

    return render(request,
                  'main/edit_transportation_admin.html',
                  {"competition": competition,
                   "registrations": registrations,
                   "transportations": transportations,
                   'drivers': drivers,
                   'passengers': passengers,
                   })


@login_required
def transportation_public(request, id):
    competition = Competition.objects.get(pk=id)
    registrations = Registration.objects.filter(competition=id)
    transportations = Transport.objects.filter(competition=id)

    return render(request,
                  'main/transportation_public.html',
                  {"competition": competition,
                   "registrations": registrations,
                   "transportations": transportations,
                   })

@login_required
@user_passes_test(is_competition_manager)
def transportation_lock_toggle(request, id):
    competition = Competition.objects.get(pk=id)
    competition.transportation_locked = not competition.transportation_locked
    competition.save()

    return redirect('main:edit_transportation_admin', id)



@require_POST
def update_note(request, pk):
    transportation = get_object_or_404(Transport, pk=pk)
    transportation.note = request.POST.get('note', '')
    transportation.save()
    return redirect('main:edit_transportation_admin', transportation.competition.id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_append_passenger(request, transport_id, registration_id):
    transport = Transport.objects.get(pk=transport_id)
    registration = Registration.objects.get(pk=registration_id)
    remove_atendee_from_transportation(transport.competition.id, registration.person.id)
    transport.passengers.add(registration.person)
    transport.save()
    return redirect("main:edit_transportation_admin", transport.competition.id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_remove_passenger(request, registration_id):
    registration = Registration.objects.get(pk=registration_id)
    remove_atendee_from_transportation(registration.competition.id, registration.person.id)
    return redirect("main:edit_transportation_admin", registration.competition.id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_create_driver(request, id):
    registration = Registration.objects.get(pk=id)
    remove_atendee_from_transportation(registration.competition.id, registration.person.id)
    available_seats = max(0, registration.transport - 1) if registration.transport in range(2, 8) else 0
    transport = Transport.objects.create(competition=registration.competition, driver=registration.person, available_seats=available_seats)
    transport.save()
    return redirect("main:edit_transportation_admin", transport.competition.id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_remove_driver(request, id):
    transport = Transport.objects.get(pk=id)
    transport.delete()
    return redirect("main:edit_transportation_admin", transport.competition.id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_publish(request, id):
    competition = Competition.objects.get(pk=id)
    competition.transportation_done = True
    competition.save()
    return redirect("main:edit_transportation_admin", id)


@login_required
@user_passes_test(is_competition_manager)
def transportation_generate(request, id):
    transports = Transport.objects.filter(competition=id)
    if not transports:
        registrations = Registration.objects.filter(competition=id)
        rnr = [] # registrations needing a ride
        seats_available = 0 # seat available for the whole process
        avail_transports = [] # available transports - cars that have free seats

        # first create list of people needing a ride - rnr
        for registration in registrations:
            if registration.transport == 1:
                rnr.append(registration)

        # now look for drivers and create driver transports with passengers that both has selected each other
        for registration in registrations:
            if registration.transport >= 2:
                passengers_r = [p for p in rnr if p.person in registration.preferred_passengers.all() and p.transport == 1 and p.preferred_driver == registration.person]
                available_seats = max(0, registration.transport - 1) if registration.transport in range(2, 8) else 0
                if len(passengers_r) > available_seats:
                    print("Too many passengers for driver", registration.person)
                    messages.error(request, _("Too many passengers for driver ") + str(registration.person))
                    passengers_r = []
                seats_available += available_seats - len(passengers_r)
                transport = Transport.objects.create(competition=registration.competition, driver=registration.person,
                                                     available_seats=available_seats)
                # get list of persons from passengers_r
                passengers = [p.person for p in passengers_r]
                # remove passengers from rnr
                rnr = [p for p in rnr if p.person not in passengers]
                transport.passengers.set(passengers)
                txt = _("Car created, driver ") + " " + str(registration.person) + " " + _("with") + " " + str(available_seats) + " " + _("seats") + " " + _("and") + " " + str(len(passengers)) + " " + _("passengers: ") + ", ".join([str(p) for p in passengers])
                messages.info(request, txt)
                print(txt)
                transport.save()
                if available_seats > 0:
                    avail_transports.append(transport)

        # loop over drivers again and try to assign remaining preferred passengers to them
        for registration in registrations:
            if registration.transport >= 2:
                passengers_r = [p for p in rnr if p.person in registration.preferred_passengers.all() and p.transport == 1 and p.preferred_driver != registration.person]
                for psgr in passengers_r:
                    for transport in avail_transports:
                        if transport.driver == registration.person:
                            if transport.available_seats >= transport.passengers.count() + len(passengers_r):
                                transport.passengers.add(psgr.person)
                                transport.save()
                                txt = _("Passenger ") + str(psgr.person) + _(" added to driver ") + str(transport.driver)
                                messages.info(request, txt)
                                print(txt)
                                rnr = [p for p in rnr if p.person != psgr.person]
                            else:
                                txt = _("Passenger ") + str(psgr.person) + _(" does not fit into the car of ") + str(registration.person)
                                print(txt)
                                messages.error(request, txt)
                                continue


        # loop over remaining registrations needing a ride and assign them to available transports if they selected a preferred driver
        rrnr = [] # remaining registrations needing a ride
        for registration in rnr:
            registered = False
            if registration.preferred_driver:
                for transport in avail_transports:
                    if not registered:
                        if transport.driver == registration.preferred_driver:
                            if transport.available_seats >= transport.passengers.count() + 1:
                                transport.passengers.add(registration.person)
                                transport.save()
                                txt = _("Passenger ") + str(registration) + _(" added to driver ") + str(transport.driver)
                                messages.info(request, txt)
                                print(txt)
                                registered = True
                                # go to next registration
                                continue
                            else:
                                txt = _("Passenger ") + str(registration) + _(" does not fit into the car of ") + str(transport.driver)
                                print(txt)
                                messages.error(request, txt)
                                continue
            if not registered:
                rrnr.append(registration)

        txt = _("Available seats overall ") + str(seats_available) + _(" and people looking for a ride: ") + str(len(rnr))
        messages.info(request, txt)
        print(txt)

        # loop over avail_transports and remove if they are full
        for transport in avail_transports:
            if transport.available_seats == transport.passengers.count():
                avail_transports.remove(transport)

        # now look in rrnr and set them in available transports
        srrnr = [reg for reg in rrnr] # super remaining registrations needing a ride
        for reg in rrnr:

            avail_transports = sorted(avail_transports, key=lambda x: (x.available_seats - x.passengers.count()), reverse=True)
            print("Sorted avail_transports", ", ".join(str(t) + str(t.driver) + "-"+ str(t.available_seats - t.passengers.count()) for t in avail_transports))
            if avail_transports:
                cur_trans = avail_transports[0]
                cur_trans.passengers.add(reg.person)
                cur_trans.save()
                txt = _("Passenger ") + str(reg) + _(" added to driver ") + str(cur_trans.driver)
                messages.info(request, txt)
                srrnr.remove(reg)
                print(txt)

                if cur_trans.available_seats == cur_trans.passengers.count():
                    avail_transports.remove(cur_trans)

        if len(srrnr) > 0:
            txt = _("Could not assign ") + str(len(srrnr)) + _(" people in total: ") + ", ".join([str(r) for r in srrnr])
            messages.error(request, txt)
            print(txt)
    else:
        print(_("Transports already exist for competition"), id)
        messages.error(request, _("Transports already exist for competition") + " " + str(id))

    return redirect("main:edit_transportation_admin", id)


@login_required
@user_passes_test(is_competition_manager)
def clear_transportation(request, id):
    # get all transports for competition
    transports = Transport.objects.filter(competition=id)
    # delete all transports
    for transport in transports:
        transport.delete()
    return redirect("main:edit_transportation_admin", transport.competition.id)


def set_available_seats(request, transportation_id, seats):
    transportation = get_object_or_404(Transport, pk=transportation_id)
    transportation.available_seats = seats
    transportation.save()
    return redirect('main:edit_transportation_admin', transportation.competition.id)


@login_required
@user_passes_test(is_medic)
def verify_exam(request, exam_id):
    medical_examination = MedicalExamination.objects.get(pk=exam_id)
    first_name = medical_examination.person.first_name
    last_name = medical_examination.person.last_name
    date_until = medical_examination.examination_until.isoformat()
    subject = 'Tvá zdravotní prohlídka byla přijata - Your medical examination was accepted.'
    message = 'Ahoj - Hello,' + '\n' + \
              'Tvá zdravotní prohlídka byla přijata - Your medical examination was accepted.' + '\n' + \
              'Email byl vegenerován automaticky - Email was generated automatically.'
    if medical_examination.person.hosted_member:
        subject = 'Důležitá infomace ke zdravoní prohlídce - Important note to your medical examination'
        message = ('Tvá prohlídka byla sice úspěšně uložena do Informačního systému I.PKO, ale aby ses mohl/a '
                   'přihlašovat na soutěže ČSPS, musíš zajistit, aby Tvá prohlídka byla uložena i do Informačního '
                   'systému ČSPS. To může učinit jen oprávněná osoba Tvého mateřského klubu (v I.PKO jsi na '
                   'hostování). Kontaktuj prosím neprodleně takovou osobu ve svém mateřském klubu a požádej ji o '
                   'zajištění uvedeného.' + '\n' +
                   'Although your medical examination has been successfully saved in the I.PKO Information System, '
                   'in order to register for ČSPS competitions, you must ensure that your '
                   'medical examination is also saved in the ČSPS Information System. Only '
                   'an authorized person from your home club can do this (you are hosted in '
                   'I.PKO). Please contact such a person in your home club immediately and '
                   'ask them to arrange this.'
                   )
        messages.info(request,
                      _(f"Your medical examination cannot be synced to ČSPS; only your home club can do this. Please contact them to arrange it."))
    elif medical_examination.person.status == MemberStatus.Aspirant:
        message = 'Ahoj - Hello,' + '\n' + \
                  'Zdravotní prohlídka aspiranta byla přijata:' + medical_examination.person + '\n' + \
                  'Email byl vegenerován automaticky - Email was generated automatically.'
        messages.info(request, _(f"Medical examination accepted for aspirant."))
    else:
        update_user_medical_examination_on_IS(first_name, last_name, medical_examination.person.iscsps_id, date_until)
        messages.info(request, _("Medical examination accepted in IS I.PKO."))

    MedicalExamination.objects.filter(pk=exam_id).update(verified=True)

    email_from = settings.EMAIL_HOST_USER
    recipient_list = [medical_examination.person.aspirant_manager.email] if medical_examination.person.status == MemberStatus.Aspirant else [medical_examination.person.email]
    send_mail(subject, message, email_from, recipient_list)

    return redirect("main:medical_exams")


@login_required
@user_passes_test(is_brigade_role)
def verify_brigade(request, brigade_id):
    person = get_current_person(request)
    Brigade.objects.filter(pk=brigade_id).update(verified=True, accountable=person)
    return redirect("main:brigade_requests")


@login_required
@user_passes_test(is_hr)
def list_aspirants(request, person_id):
    membership = FulfilledMembership.objects.filter(
        Q(person__status=MemberStatus.Aspirant) | Q(person__status=MemberStatus.Accepting),
        year=date.today().year,
        person__user__is_active=True,
        person__aspirant_manager=person_id,
    ).select_related('person').order_by(
        'person__last_name', 'person__first_name', 'person__birth_date')

    persons = Person.objects.filter(
        Q(status=MemberStatus.Aspirant) | Q(status=MemberStatus.Accepting),
        user__is_active=True,
        aspirant_manager=person_id).annotate(
        latest_examination=Max(
            'medicalexamination__examination_until',
            filter=Q(medicalexamination__verified=True)),
        days_since_member=ExpressionWrapper(
            date.today() - F('member_since'),
            output_field=fields.DurationField()),
    ).order_by('last_name', 'first_name', 'person__birth_date')

    eligibles = []
    for person in persons:
        ms = FulfilledMembership.objects.get(person=person, year=date.today().year)
        eligible = person.is_aspirant and person.small_reg_form and person.big_reg_form and person.gdpr_agreed \
                   and person.information_agreed and ms.fulfilled_membership and (
                           person.latest_examination is not None and \
                           person.latest_examination > date.today())
        eligibles.append(eligible)

    assert len(persons) == len(membership)

    group = Group.objects.get(name='HR role')
    if group is not None:
        all_managers = group.user_set.all()
        manager_list = Person.objects.filter(user__in=all_managers).order_by('last_name')
    current_manager = Person.objects.filter(pk=person_id)[0]

    return render(request,
                  'main/aspirants.html',
                  {"data": zip(persons, membership, eligibles),
                   'current_date': date.today(),
                   'managers': manager_list,
                   'current_manager': current_manager})


@login_required
@user_passes_test(is_hr)
def list_members(request):
    persons = Person.objects.filter(Q(status=MemberStatus.Member) | Q(status=MemberStatus.Escaping),
                                    user__is_active=True).order_by('last_name', 'first_name', 'person__birth_date')
    current_date = date.today()
    if current_date.month < 4:
        season_year = current_date.year - 1
    else:
        season_year = current_date.year
    fulfilled_memberships = FulfilledMembership.objects \
        .filter(Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping),
                year=season_year,
                person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')
    fulfilled_brigade_act = FulfilledBrigade.objects \
        .filter(Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping),
                year=season_year,
                person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')
    fulfilled_brigade_prev = FulfilledBrigade.objects \
        .filter(Q(person__status=MemberStatus.Member) | Q(person__status=MemberStatus.Escaping),
                year=season_year - 1,
                person__user__is_active=True) \
        .select_related('person').order_by('person__last_name', 'person__first_name', 'person__birth_date')

    persons = Person.objects.filter(Q(status=MemberStatus.Member) | Q(status=MemberStatus.Escaping),
                                    user__is_active=True).annotate(
        latest_examination=Max(
            'medicalexamination__examination_until',
            filter=Q(medicalexamination__verified=True))
    ).order_by('last_name', 'first_name', 'person__birth_date')

    assert len(persons) == len(fulfilled_brigade_act)
    assert len(persons) == len(fulfilled_brigade_prev)
    assert len(persons) == len(fulfilled_memberships)

    return render(request,
                  'main/hr.html',
                  {"data": zip(persons, fulfilled_memberships, fulfilled_brigade_act, fulfilled_brigade_prev),
                   'season_year': season_year,
                   'current_date': date.today()})


@login_required
@user_passes_test(is_medic)
def medical_exams(request):
    if request.method == 'POST':
        form = MedicalExaminationRejectionForm(request.POST)
        exam_id = request.POST.get('examination_id')
        exam_obj = MedicalExamination.objects.get(pk=exam_id)
        person = Person.objects.get(pk=exam_obj.person.pk)

        subject = 'Zdravotní prohlídka byla zamítnuta - Medical examination rejected'
        message = 'Tvoje zdravotní prohlídka byla zamítnuta. Nahraj prosím novou.' + '\n' \
                  + "Důvod: " + form['note'].value() + '\n' + '\n' \
                  + 'Your medical examination was rejected. Please, upload new one.' + '\n' \
                  + "Reason: " + form['note'].value() \
                  + '\n' + 'Email was generated automatically.'
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, [person.email])
        exam_obj.rejected = True
        exam_obj.rejected_note = form['note'].value()
        exam_obj.save()

        return HttpResponseRedirect(request.path_info)

    form = MedicalExaminationRejectionForm()
    medical_exams_to_verify = MedicalExamination.objects.filter(verified=False, rejected=False).order_by(
        'examination_until')
    file_exists = []
    for m in medical_exams_to_verify:
        try:
            m.examination_file_link.url
            file_exists.append(True)
        except ValueError:
            file_exists.append(False)

    rejected_exams = MedicalExamination.objects.filter(rejected=True).order_by('examination_until')
    rejected_file_exists = []
    for m in rejected_exams:
        try:
            m.examination_file_link.url
            rejected_file_exists.append(True)
        except ValueError:
            rejected_file_exists.append(False)

    return render(request,
                  'main/medical_exams.html',
                  {"exams": zip(medical_exams_to_verify, file_exists),
                   'form': form,
                   "rejected_exams": zip(rejected_exams, rejected_file_exists), })


@login_required
@user_passes_test(is_brigade_role)
def brigade_requests(request):
    person = get_current_person(request)

    brigades_to_verify = Brigade.objects.filter(verified=False).exclude(accountable=person)
    requests_for_me = Brigade.objects.filter(verified=False, accountable=person)

    return render(request,
                  'main/brigade_requests.html',
                  {"data": brigades_to_verify,
                   "requests_for_me": requests_for_me})


@login_required
def add_brigade(request):
    if request.method == 'POST':
        form = BrigadeForm(request.POST)
        brigade = form.save(commit=False)
        brigade.person = get_current_person(request)
        if form.is_valid():
            form.save()
            return redirect("main:user_info")
    else:
        form = BrigadeForm()

    return render(request=request,
                  template_name="main/add_brigade.html",
                  context={'form': form})


@login_required
def register(request):
    person = Person.objects.filter(user=request.user).first()
    active_competitions = Competition.objects.filter(signing_from__lte=date.today(),
                                                     signing_until__gte=date.today(),
                                                     )
    my_registrations = Registration.objects.filter(person=person,
                                                   competition__signing_from__lte=date.today(),
                                                   competition__event_date__gte=date.today())
    my_comp_ids = [a['competition'] for a in my_registrations.values('competition')]

    available_competitions_to_me = active_competitions.exclude(id__in=my_comp_ids)

    available_competitions_to_me = [comp for comp in available_competitions_to_me if
                                    datetime.combine(comp.signing_until,
                                                     datetime.strptime('19:00', '%H:%M').time()) > datetime.now()]

    # Extract IDs of available competitions
    available_competition_ids = [comp.id for comp in available_competitions_to_me]

    upcoming_competitions = Competition.objects.filter(event_date__gte=date.today()).order_by('event_date')
    upcoming_competitions = upcoming_competitions.exclude(id__in=available_competition_ids)
    upcoming_competitions = upcoming_competitions.exclude(id__in=my_comp_ids)

    warnings = [person.check_eligibility(d.event_date) for d in available_competitions_to_me]
    person_is_eligible = [len(warning) == 0 for warning in warnings]

    registration_removable = [date.today() >= r.competition.signing_from and date.today() <= r.competition.signing_until
                              for r in my_registrations]
    return render(request,
                  'main/register.html',
                  {"competitions_eligibility_warnings": zip(available_competitions_to_me, person_is_eligible, warnings),
                   "my_registrations": zip(my_registrations, registration_removable),
                   "today": date.today(),
                   "upcoming_competitions": upcoming_competitions,
                   "transports": transports,
                   })


@login_required
def unregister(request, id):
    person = Person.objects.filter(user=request.user).first()
    registration = Registration.objects.get(pk=id)

    if registration.person == person:
        for lenght in registration.length.all():
            print(f"Deleting {registration.competition} {person} {lenght}")
            if delete_competition_application(registration.competition.iscsps_id, person.id, str(lenght), registration.championship_course):
                messages.info(request, _("Unregistered successfully:") + f" {lenght}")
            else:
                messages.error(request, _("Unregistration failed:") + f" {lenght}")
        registration.delete()

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def register_to(request, id):
    competition = Competition.objects.get(pk=id)
    person = get_current_person(request)
    person_track_limit = person.get_maximal_allowed_competition_lenght(competition)
    lengths = competition.lengths.filter(length__lte=person_track_limit).order_by('length')
    expected_temp_under4deg = competition.expected_temp == '4-'
    cs_rules = ChampionshipCourseRule.objects.filter(temp_under4=expected_temp_under4deg,
                                                     swimmercategory=person.get_swimmercategory())
    cs_length = cs_rules[0].length.pk if cs_rules[0].length.length <= person_track_limit else 0
    message = f"Doplatek {competition.event_venue} {competition.event_date.year} {person.first_name} {person.last_name} {person.birth_date.year}"
    qr_link = f"https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=6147514003&bankCode=5500&amount={competition.copayment}&currency=CZK&message={message}"

    form = RegistrationForm(request.POST or None,
                            request.FILES or None,
                            competition=competition,
                            person=person,
                            lengths=lengths,
                            track_editable=True,
                            cs_length=cs_length
                            )

    context = {
        'form': form,
        'comp': competition,
        'cs_length': cs_length,
        'lengths': lengths,
        'qr_link': qr_link,
        'copayment_message': message,
        'copayment_amount': competition.copayment,
        'button_label': _('Register'),
    }

    if request.method == "POST":
        # check if already registered
        if Registration.objects.filter(person=person, competition=competition).count() > 0:
            messages.error(request, _("You are already registered to this competition."))
            return redirect("main:register")
        if form.is_valid():
            messages.info(request, _("Registered successfully!"))
            form.save()
            # registered sussessfully, put application on csps and redirect
            csc_descr = ""
            for lenght in form.cleaned_data['length']:
                if competition.is_championship_venue:
                    championship_course = form.cleaned_data['championship_course']
                    if championship_course:
                        csc_descr = " (" + _("CSC") + ")"
                    print(f"Applying for {competition} {person} {lenght} - Mistrovka trat: {championship_course}")
                    apply_for_competition(competition.iscsps_id, person.id, str(lenght), championship_course)
                else:
                    print(f"Applying for {competition} {person} {lenght}")
                    apply_for_competition(competition.iscsps_id, person.id, str(lenght))
            # send email
            email = [person.email]
            subject = _('You applied for competition ') + str(competition)
            message = ('\n' + _('You applied for competition ') + str(competition) + '.\n' + _(
                'Your track length is ') + ', '.join(map(str, form.cleaned_data['length'])) + csc_descr + '\n' + _(
                'For details see your profile on Information Centre of I. PKO.') + '\n\n' + _(
                'Email was generated automatically.'))
            email_from = settings.EMAIL_HOST_USER
            send_mail(subject, message, email_from, email)
            return redirect("main:register")
        else:
            return render(request, 'main/register_form.html', context)

    return render(request, 'main/register_form.html', context)


@login_required
def edit_registration(request, registration_id):
    registration = get_object_or_404(Registration, pk=registration_id)
    initial_lengths = list(registration.length.all())
    initial_championship_course = registration.championship_course

    if request.method == 'POST':
        competition = registration.competition
        person = get_current_person(request)
        person_track_limit = person.get_maximal_allowed_competition_lenght(competition)
        lengths = competition.lengths.filter(length__lte=person_track_limit).order_by('length')
        expected_temp_under4deg = competition.expected_temp == '4-'
        cs_rules = ChampionshipCourseRule.objects.filter(temp_under4=expected_temp_under4deg,
                                                         swimmercategory=person.get_swimmercategory())
        cs_length = cs_rules[0].length.pk if cs_rules[0].length.length <= person_track_limit else 0
        track_editable = date.today() >= registration.competition.signing_from and date.today() <= registration.competition.signing_until
        message = f"Doplatek {competition.event_venue} {competition.event_date.year} {person.first_name} {person.last_name} {person.birth_date.year}"
        qr_link = f"https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=6147514003&bankCode=5500&amount={competition.copayment}&currency=CZK&message={message}"

        form = RegistrationForm(request.POST or None,
                                request.FILES or None,
                                competition=registration.competition,
                                person=person,
                                lengths=lengths,
                                cs_length=cs_length,
                                track_editable = track_editable,
                                instance=registration,
                                )
        if form.is_valid():
            new_lengths = form.cleaned_data['length']
            added_lengths = set(new_lengths) - set(initial_lengths)
            removed_lengths = set(initial_lengths) - set(new_lengths)
            csc_descr = ""

            # Handle removed lengths
            for length in removed_lengths:
                print(f"Removed length: {length}")
                if competition.is_championship_venue:
                    print(f"Removed length: {length} - Mistrovka trat: {initial_championship_course}")
                    delete_competition_application(competition.iscsps_id, person.id, str(length), initial_championship_course)
                else:
                    delete_competition_application(competition.iscsps_id, person.id, str(length))

            # Handle added lengths
            for length in added_lengths:
                print(f"Added length: {length}")
                if competition.is_championship_venue:
                    apply_for_competition(competition.iscsps_id, person.id, str(length), form.cleaned_data['championship_course'])
                    if form.cleaned_data['championship_course']:
                        csc_descr = " (" + _("CSC") + ")"
                else:
                    apply_for_competition(competition.iscsps_id, person.id, str(length))

            if competition.is_championship_venue:
                if initial_championship_course != form.cleaned_data['championship_course'] and initial_lengths[0] == new_lengths[0]:
                    delete_competition_application(competition.iscsps_id, person.id, str(initial_lengths[0]), initial_championship_course)
                    apply_for_competition(competition.iscsps_id, person.id, str(new_lengths[0]), form.cleaned_data['championship_course'])
                    if form.cleaned_data['championship_course']:
                        csc_descr = " (" + _("CSC") + ")"

            form.save()
            messages.info(request, _("Registration changed successfully!"))
            # send email
            email = [person.email]
            subject = _('You changed your application for competition ') + str(competition)
            message = ('\n' + _('You are applied for competition ') + str(competition) + '.\n' + _(
                'Your new track length is ') + ', '.join(map(str, form.cleaned_data['length'])) + csc_descr + '\n' + _(
                'For details see your profile on Information Centre of I. PKO.') + '\n\n' + _(
                'Email was generated automatically.'))
            email_from = settings.EMAIL_HOST_USER
            send_mail(subject, message, email_from, email)

            return redirect("main:register")
        else:
            return render(request, 'main/register_form.html', {'form': form,
                                                               'comp': competition,
                                                               'cs_length': cs_length,
                                                               'lengths': lengths,
                                                               'qr_link': qr_link,
                                                               'copayment_message': message,
                                                               'copayment_amount': competition.copayment,
                                                               'button_label': _('Save and override changes'),
                                                               })
    else:
        competition = registration.competition
        person = get_current_person(request)
        person_track_limit = person.get_maximal_allowed_competition_lenght(competition)
        lengths = competition.lengths.filter(length__lte=person_track_limit).order_by('length')
        expected_temp_under4deg = competition.expected_temp == '4-'
        cs_rules = ChampionshipCourseRule.objects.filter(temp_under4=expected_temp_under4deg,
                                                         swimmercategory=person.get_swimmercategory())
        cs_length = cs_rules[0].length.pk if cs_rules[0].length.length <= person_track_limit else 0
        message = f"Doplatek {competition.event_venue} {competition.event_date.year} {person.first_name} {person.last_name} {person.birth_date.year}"
        qr_link = f"https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=6147514003&bankCode=5500&amount={competition.copayment}&currency=CZK&message={message}"

        track_editable = date.today() >= registration.competition.signing_from and date.today() <= registration.competition.signing_until
        message = f"Doplatek {competition.event_venue} {competition.event_date.year} {person.first_name} {person.last_name} {person.birth_date.year}"
        qr_link = f"https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=6147514003&bankCode=5500&amount={competition.copayment}&currency=CZK&message={message}"

        form = RegistrationForm(request.POST or None,
                                request.FILES or None,
                                competition=competition,
                                person=person,
                                lengths=lengths,
                                cs_length=cs_length,
                                track_editable=track_editable,
                                instance=registration
                                )
    return render(request, 'main/register_form.html', {'form': form,
                                                       'comp': competition,
                                                       'cs_length': cs_length,
                                                       'lengths': lengths,
                                                       'qr_link': qr_link,
                                                       'copayment_message': message,
                                                       'copayment_amount': competition.copayment,
                                                       'button_label': _('Save and override changes'),
                                                       })


def api_competitions(request):
    competitions = Competition.objects.filter(event_date__gte=date.today(),
                                              signing_from__lte=date.today())
    competition_list = list(competitions.values())
    venues = [{'venue': competition.event_venue.location} for competition in competitions]
    for dato, venue in zip(competition_list, venues):
        dato.update(venue)

    return HttpResponse(json.dumps(competition_list, indent=4, sort_keys=True, default=str),
                        content_type="application/json")


def get_latest_examination(person):
    try:
        return MedicalExamination.objects.filter(person=person,
                                                 verified=True).latest('examination_until').examination_until
    except ObjectDoesNotExist:
        return None


def api_examinations(request):
    persons_list = Person.objects.filter(user__is_active=True)

    today = date.today()
    datas = [{'first_name': person.first_name,
              'last_name': person.last_name,
              'birth_year': person.birth_date.year,
              'examination_until': get_latest_examination(person),
              'id_iscps': person.iscsps_id} for person in persons_list]

    return HttpResponse(json.dumps(datas, indent=4, sort_keys=True, default=str, ensure_ascii=False),
                        content_type="application/json")


def api_registrations(request, iscsps_id):
    registrations = Registration.objects.filter(competition__iscsps_id=iscsps_id)

    data_list = []
    for registration in registrations:
        lengths = [a.length for a in registration.length.all()]

        if registration.championship_course:
            modified_lengths = [(length * 10 + 1) for length in lengths]
        else:
            modified_lengths = lengths

        if any(value % 1 != 0 for value in modified_lengths):
            modified_lengths = [int(value) if value % 1 == 0 else value for value in modified_lengths]
        else:
            modified_lengths = [int(value) for value in modified_lengths]

        data_list.append({
            'length': modified_lengths,
            'first_name': registration.person.first_name,
            'last_name': registration.person.last_name,
            'birth_year': registration.person.birth_date.year,
        })

    return HttpResponse(json.dumps(data_list, indent=4, sort_keys=True, default=str),
                        content_type="application/json")


@login_required
def payment_history(request, person_id):
    swimmer = Person.objects.get(pk=person_id)
    payments = Payment.objects.filter(person=swimmer).order_by('-date')
    return render(request, 'main/payment_history.html', {'swimmer': swimmer,
                                                         'payments': payments})


@login_required
def brigade_history(request, person_id):
    swimmer = Person.objects.get(pk=person_id)
    brigade_list = Brigade.objects.filter(person=swimmer).order_by('-date')
    return render(request, 'main/brigade_history.html', {'swimmer': swimmer,
                                                         'brigades': brigade_list})


@login_required
def payment_details(request, fee_type, year, amount, optional_text=None):
    person = get_current_person(request)
    first_name = person.first_name
    last_name = person.last_name
    birth_year = person.birth_date.year
    year = int(year)

    if fee_type == "CP":
        message = f"Členský příspěvek PKO {year} {first_name} {last_name} {birth_year}"
    elif fee_type == "Br":
        message = f"Brigáda 07/{year}-06/{year + 1} {last_name} {birth_year}"
    elif fee_type == "Co-pay":
        message = f"Doplatek {optional_text} {year} {first_name} {last_name} {birth_year}"
    qr_link = f"https://api.paylibo.com/paylibo/generator/czech/image?accountNumber=6147514003&bankCode=5500&amount={amount}&currency=CZK&message={message}"

    data = {'amount': amount,
            'message': message,
            'link': qr_link}
    return render(request, 'main/payment_details.html', data)


@login_required
def medical_exams_all(request):
    persons = Person.objects.filter(user__is_active=True).annotate(
        latest_examination=Max(
            'medicalexamination__examination_until',
            filter=Q(medicalexamination__verified=True))
    ).order_by('last_name', 'first_name', 'person__birth_date')
    return render(request, 'main/medical_exams_all.html', {'persons': persons})


@login_required
def medical_exams_history(request, person_id):
    swimmer = Person.objects.get(pk=person_id)
    me = MedicalExamination.objects.filter(person=swimmer, verified=True).order_by('-examination_until')
    return render(request, 'main/medical_exams_history.html', {'swimmer': swimmer,
                                                               'medical_exams': me})


@login_required
def add_new_member_cus(request, pk):
    person = Person.objects.get(pk=pk)

    add_new_member_to_CUS(first_name=person.first_name,
                          last_name=person.last_name,
                          rodne_cislo=person.RC,
                          ulice=person.adr_street,
                          cislo_popisne=person.adr_home_number_popisne,
                          cislo_orientacni=person.adr_home_number2_orientacni,
                          psc=person.adr_post_code,
                          mesto=person.adr_city,
                          phone=person.phone_number,
                          email=person.email,
                          )


@login_required
def list_new_members(request):
    # get people with active=True
    persons = Person.objects.filter(user__is_active=True).order_by('last_name', 'first_name', 'person__birth_date')

    # build csv of person using last_name, first_name, birth_date and indication if he is member less then a year
    persons_list = []
    for person in persons:
        if person.member_since is not None:
            is_member_less_than_year = date.today() - person.member_since < timedelta(days=365)
        else:
            is_member_less_than_year = False

        persons_list.append(
            f'"{person.last_name}","{person.first_name}","{person.birth_date.year}","{"N" if is_member_less_than_year else "S"}"')

    output_str = '\n'.join(persons_list)

    response = HttpResponse(output_str, content_type='text/plain; charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="members_list.csv"'
    return response


@login_required
@user_passes_test(is_hr)
def accept_aspirant(request, pk):
    person = Person.objects.get(pk=pk)
    birth_date = person.birth_date.isoformat()
    # TODO: check if person is already in CSPS, handle hosting members
    if person.is_czech_citizenship and person.status != MemberStatus.Accepting:
        person.status = MemberStatus.Accepting
        person.save()
        iscsps_id = add_new_member_to_CSPS(first_name=person.first_name,
                                           last_name=person.last_name,
                                           rodne_cislo=person.RC,
                                           ulice=person.adr_street,
                                           cislo_popisne=person.adr_home_number_popisne,
                                           psc=person.adr_post_code,
                                           cislo_orientacni=person.adr_home_number2_orientacni,
                                           mesto=person.adr_city,
                                           sex=str(person.sex).upper(),
                                           birth_date=birth_date,
                                           )

        if iscsps_id is not None and iscsps_id > 0:
            messages.info(request, _("CSPS ID found"))
            person.iscsps_id = iscsps_id
            person.save()
            medical_examination = MedicalExamination.objects.filter(
                person=person).latest('examination_until')
            date_until = medical_examination.examination_until.isoformat()
            update_user_medical_examination_on_IS(first_name=person.first_name, last_name=person.last_name,
                                                  id=iscsps_id, date_until=date_until)

            add_new_member_to_CUS(first_name=person.first_name,
                                  last_name=person.last_name,
                                  rodne_cislo=person.RC,
                                  ulice=person.adr_street,
                                  cislo_popisne=person.adr_home_number_popisne,
                                  cislo_orientacni=person.adr_home_number2_orientacni,
                                  psc=person.adr_post_code,
                                  mesto=person.adr_city,
                                  phone=person.phone_number,
                                  email=person.email,
                                  )

            person.status = MemberStatus.Member
            person.address = f"{person.adr_street if person.adr_street else person.adr_city} {person.adr_home_number_popisne}{'/' + person.adr_home_number2_orientacni if person.adr_home_number2_orientacni else ''}, {person.adr_post_code} {person.adr_city}"
            person.member_since = date.today()
            person.save()

            recipient_list = [person.aspirant_manager.email]
            messages.info(request, _("User accepted"))
            subject = _('Aspirant accepted into the club')
            message = _('New club member was just accepted into club (including all relevant databases): ') + str(
                person) + '\n' + _('Best regards, IS I. PKO')
            email_from = settings.EMAIL_HOST_USER
            send_mail(subject, message, email_from, recipient_list)

        else:
            messages.info(request, _("ID not found"))

        current_user_id = get_current_person(request).pk
        return redirect(reverse("main:list_aspirants", kwargs={'person_id': current_user_id}))


@login_required
@user_passes_test(is_hr)
def add_aspirant(request):
    person = get_current_person(request)

    if request.method == "POST":
        form = AspirantForm(request.POST, as_manager=person)
        if form.is_valid():
            form.save()
            messages.info(request, _("Aspirant added successfully!"))
            return redirect('main:list_aspirants', person_id=person.pk)
        else:
            print(form.errors)
            return render(request, 'main/aspirant_form.html', context={'form': form})

    form = AspirantForm(as_manager=person)
    return render(request, 'main/aspirant_form.html', context={'form': form})


@login_required
@user_passes_test(is_hr)
def edit_aspirant(request, pk):
    aspirant = get_object_or_404(Person, pk=pk)

    if request.method == "POST":
        form = AspirantForm(request.POST, instance=aspirant)
        if form.is_valid():
            form.save()
            messages.info(request, _("Aspirant updated successfully!"))
            return redirect('main:list_aspirants', person_id=aspirant.aspirant_manager.pk)
        else:
            print(form.errors)
            return render(request, 'main/aspirant_form.html', context={'form': form})

    form = AspirantForm(instance=aspirant, as_manager=aspirant.aspirant_manager)
    return render(request, 'main/aspirant_form.html', context={'form': form})


@login_required
def toggle_co_paid(request, competition_id, pk):
    registration = get_object_or_404(Registration, pk=pk)
    registration.co_paid = not registration.co_paid
    registration.save()
    return redirect('main:list_attendees', id=competition_id)


@login_required
def toggle_aspirant_temp_registration(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.temp_registration = not person.temp_registration
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@login_required
def toggle_aspirant_big_registration(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.big_reg_form = not person.big_reg_form
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@login_required
def toggle_aspirant_small_registration(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.small_reg_form = not person.small_reg_form
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@login_required
def toggle_aspirant_gdpr(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.gdpr_agreed = not person.gdpr_agreed
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@login_required
def toggle_aspirant_information_agreed(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.information_agreed = not person.information_agreed
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@login_required
def toggle_aspirant_pictures2pcs(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    person.pictures2pcs = not person.pictures2pcs
    person.save()
    return redirect('main:list_aspirants', person_id=person.aspirant_manager.pk)


@require_POST
def aspirant_update_note(request, pk):
    aspirant = get_object_or_404(Person, pk=pk)
    aspirant.aspirant_note = request.POST.get('note', '')
    aspirant.save()
    return redirect('main:list_aspirants', person_id=aspirant.aspirant_manager.pk)


@login_required
def send_payment_email(request, person_id):
    person = get_object_or_404(Person, pk=person_id)
    group = Group.objects.filter(name='Treasurer Role').first()
    if group is not None:
        all_recipients = group.user_set.filter(is_superuser=False)
        recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
    else:
        recipient_list = []

    # Add aspirant manager's email to the recipient list
    if person.aspirant_manager and person.aspirant_manager.email:
        recipient_list.append(person.aspirant_manager.email)

    if request is not None:
        subject = 'Zapsani platby'
        message = 'Ahoj,\n\n' \
                  'Prosim o zapsani platby aspirantovi ' + person.first_name + ' ' + person.last_name + '\n\nIS I. PKO'
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)

    return redirect('main:list_aspirants', person.aspirant_manager.pk)


class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    template_name = 'main/users/password_reset.html'
    email_template_name = 'main/emails/password_reset_email.html'
    subject_template_name = 'main/emails/password_reset_subject.html'
    success_message = _(
        "We've emailed you instructions for setting your password, if an account exists with the email you entered. You should receive them shortly. If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder.")
    success_url = reverse_lazy('main:homepage')

