# Generated by Django 3.2.25 on 2024-11-13 13:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0025_auto_20241111_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='transport',
            name='available_seats',
            field=models.IntegerField(default=0, verbose_name='Available seats'),
        ),
        migrations.AddField(
            model_name='transport',
            name='note',
            field=models.TextField(blank=True, default='', null=True, verbose_name='Poznámka'),
        ),
        migrations.AlterField(
            model_name='registration',
            name='preferred_passengers',
            field=models.ManyToManyField(blank=True, related_name='preferred_passengers', to='main.Person', verbose_name='Preferred Passengers'),
        ),
    ]
