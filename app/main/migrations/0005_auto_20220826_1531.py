# Generated by Django 3.2.15 on 2022-08-26 15:31

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_remove_fulfilledmembership_fulfilled_brigade_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historicalbrigade',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical brigade', 'verbose_name_plural': 'historical brigades'},
        ),
        migrations.AlterModelOptions(
            name='historicalbrigadework',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical brigade work', 'verbose_name_plural': 'historical brigade works'},
        ),
        migrations.AlterModelOptions(
            name='historicalcompetition',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical competition', 'verbose_name_plural': 'historical competitions'},
        ),
        migrations.AlterModelOptions(
            name='historicalcompetitionlength',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical competition length', 'verbose_name_plural': 'historical competition lengths'},
        ),
        migrations.AlterModelOptions(
            name='historicalcompetitionvenue',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical competition venue', 'verbose_name_plural': 'historical competition venues'},
        ),
        migrations.AlterModelOptions(
            name='historicalmedicalexamination',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical medical examination', 'verbose_name_plural': 'historical medical examinations'},
        ),
        migrations.AlterModelOptions(
            name='historicalmembershipfees',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical membership fees', 'verbose_name_plural': 'historical membership feess'},
        ),
        migrations.AlterModelOptions(
            name='historicalmoneytobrigaderate',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical money to brigade rate', 'verbose_name_plural': 'historical money to brigade rates'},
        ),
        migrations.AlterModelOptions(
            name='historicalpayment',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical payment', 'verbose_name_plural': 'historical payments'},
        ),
        migrations.AlterModelOptions(
            name='historicalperson',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical person', 'verbose_name_plural': 'historical persons'},
        ),
        migrations.AlterModelOptions(
            name='historicalregistration',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical registration', 'verbose_name_plural': 'historical registrations'},
        ),
        migrations.AddField(
            model_name='fulfilledmembership',
            name='fulfilled_brigade',
            field=models.BooleanField(default=False, verbose_name='Fulfilled'),
        ),
        migrations.AddField(
            model_name='fulfilledmembership',
            name='missing_brigade_hours',
            field=models.FloatField(default=0, verbose_name='Needed to work'),
        ),
        migrations.AddField(
            model_name='fulfilledmembership',
            name='missing_brigade_money',
            field=models.IntegerField(default=0, verbose_name='Needed to pay'),
        ),
        migrations.AlterField(
            model_name='historicalbrigade',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalbrigadework',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalcompetition',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalcompetitionlength',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalcompetitionvenue',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalmedicalexamination',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalmembershipfees',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalmoneytobrigaderate',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalpayment',
            name='due_date',
            field=models.DateField(default=datetime.date(2022, 8, 26), verbose_name='Due season year'),
        ),
        migrations.AlterField(
            model_name='historicalpayment',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='historicalregistration',
            name='history_date',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='due_date',
            field=models.DateField(default=datetime.date(2022, 8, 26), verbose_name='Due season year'),
        ),
        migrations.DeleteModel(
            name='FulfilledBrigade',
        ),
    ]
