# Generated by Django 3.2.25 on 2024-09-30 14:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20240312_2037'),
    ]

    operations = [
        migrations.AddField(
            model_name='competition',
            name='note',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Poznámka'),
        ),
        migrations.AddField(
            model_name='historicalcompetition',
            name='note',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Poznámka'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='adr_city',
            field=models.CharField(blank=True, max_length=70, null=True, verbose_name='Město'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='adr_home_number2_orientacni',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Číslo orientační'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='adr_home_number_popisne',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Číslo popisné'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='adr_post_code',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='PSČ'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='adr_street',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Ulice'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='aspirant_manager',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='main.person', verbose_name='Mentor aspiranta'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='big_reg_form',
            field=models.BooleanField(default=False, verbose_name='Velká přihláška'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='gdpr_agreed',
            field=models.BooleanField(default=False, verbose_name='Souhlas s GDPR'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='information_agreed',
            field=models.BooleanField(default=False, verbose_name='Podepsané informace ke vstupu do klubu'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='is_czech_citizenship',
            field=models.BooleanField(default=True, verbose_name='Je občanem ČR?'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='pictures2pcs',
            field=models.BooleanField(default=False, verbose_name='Dodány 2 ks fotografie'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='small_reg_form',
            field=models.BooleanField(default=False, verbose_name='Malá přihláška'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='status',
            field=models.IntegerField(choices=[(0, 'Aspirant'), (1, 'Probíhá proces zapsání člena do klubu'), (2, 'Člen'), (3, 'Činnost ukončující člen'), (4, 'Bývalý člen')], default=2, verbose_name='Status člena'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='temp_registration',
            field=models.BooleanField(default=False, verbose_name='Dočasná průkazka'),
        ),
        migrations.AlterField(
            model_name='person',
            name='adr_city',
            field=models.CharField(blank=True, max_length=70, null=True, verbose_name='Město'),
        ),
        migrations.AlterField(
            model_name='person',
            name='adr_home_number2_orientacni',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Číslo orientační'),
        ),
        migrations.AlterField(
            model_name='person',
            name='adr_home_number_popisne',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Číslo popisné'),
        ),
        migrations.AlterField(
            model_name='person',
            name='adr_post_code',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='PSČ'),
        ),
        migrations.AlterField(
            model_name='person',
            name='adr_street',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Ulice'),
        ),
        migrations.AlterField(
            model_name='person',
            name='aspirant_manager',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='Aspirants', to='main.person', verbose_name='Mentor aspiranta'),
        ),
        migrations.AlterField(
            model_name='person',
            name='big_reg_form',
            field=models.BooleanField(default=False, verbose_name='Velká přihláška'),
        ),
        migrations.AlterField(
            model_name='person',
            name='gdpr_agreed',
            field=models.BooleanField(default=False, verbose_name='Souhlas s GDPR'),
        ),
        migrations.AlterField(
            model_name='person',
            name='information_agreed',
            field=models.BooleanField(default=False, verbose_name='Podepsané informace ke vstupu do klubu'),
        ),
        migrations.AlterField(
            model_name='person',
            name='is_czech_citizenship',
            field=models.BooleanField(default=True, verbose_name='Je občanem ČR?'),
        ),
        migrations.AlterField(
            model_name='person',
            name='pictures2pcs',
            field=models.BooleanField(default=False, verbose_name='Dodány 2 ks fotografie'),
        ),
        migrations.AlterField(
            model_name='person',
            name='small_reg_form',
            field=models.BooleanField(default=False, verbose_name='Malá přihláška'),
        ),
        migrations.AlterField(
            model_name='person',
            name='status',
            field=models.IntegerField(choices=[(0, 'Aspirant'), (1, 'Probíhá proces zapsání člena do klubu'), (2, 'Člen'), (3, 'Činnost ukončující člen'), (4, 'Bývalý člen')], default=2, verbose_name='Status člena'),
        ),
        migrations.AlterField(
            model_name='person',
            name='temp_registration',
            field=models.BooleanField(default=False, verbose_name='Dočasná průkazka'),
        ),
    ]
