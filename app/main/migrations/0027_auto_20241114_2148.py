# Generated by Django 3.2.25 on 2024-11-14 21:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0026_auto_20241113_1353'),
    ]

    operations = [
        migrations.AddField(
            model_name='competition',
            name='transporatation_done',
            field=models.BooleanField(default=False, verbose_name='Transportation done'),
        ),
        migrations.AddField(
            model_name='competition',
            name='transportation_payment',
            field=models.IntegerField(default=0, verbose_name='Transportation payment'),
        ),
        migrations.AddField(
            model_name='historicalcompetition',
            name='transporatation_done',
            field=models.BooleanField(default=False, verbose_name='Transportation done'),
        ),
        migrations.AddField(
            model_name='historicalcompetition',
            name='transportation_payment',
            field=models.IntegerField(default=0, verbose_name='Transportation payment'),
        ),
    ]
