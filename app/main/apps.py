from django.apps import AppConfig
from django.core.signals import request_finished



class MainConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main'

    def ready(self):
        from . import signals
        # Implicitly connect a signal handlers decorated with @receiver.
        # Explicitly connect a signal handler.
        request_finished.connect(signals.payment_change)
        request_finished.connect(signals.brigade_change)
        request_finished.connect(signals.new_person)
        request_finished.connect(signals.new_medical_exam)
