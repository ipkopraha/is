from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.translation import gettext as _
from simple_history.models import HistoricalRecords
from .utils import get_season_year, get_season_end_year


# Create your models here.
class AgeTypes(models.IntegerChoices):
    junior = 0, _('junior')
    regular = 1, _('regular')
    senior = 2, _('senior')
    elder = 3, _('elder')

class MemberStatus(models.IntegerChoices):
    Aspirant = 0, _('Aspirant')
    Accepting = 1, _('Member acceptation in progress')
    Member = 2, _('Member')
    Escaping = 3, _('Escaping member')
    Exmember = 4, _('Exmember')



class SwimmerCategory(models.IntegerChoices):
    young_male = 0, _('Young Male')
    young_female = 1, _('Young Female')
    male_a = 2, _('Male A')
    female_a = 3, _('Female A')
    male_b = 4, _('Male B')
    female_b = 5, _('Female B')
    male_masters_a = 6, _('Male Masters A')
    female_masters_a = 7, _('Female Masters A')
    male_masters_b = 8, _('Male Masters B')
    female_masters_b = 9, _('Female Masters B')
    male_masters_c = 10, _('Male Masters C')
    female_masters_c = 11, _('Female Masters C')
    male_masters_d = 12, _('Male Masters D')
    female_masters_d = 13, _('Female Masters D')
    male_masters_e = 14, _('Male Masters E')
    female_masters_e = 15, _('Female Masters E')


def get_swimmercategory(desired: int):
    choices = [c[1] for c in SwimmerCategory.choices if c[0] == desired]
    return choices[0]


def get_swimmerage_type(desired: int):
    choices = [c[1] for c in AgeTypes.choices if c[0] == desired]
    return choices[0]


class CompetitionVenue(models.Model):
    is_homeplace = models.BooleanField(_('Is homeplace'), default=False)
    location = models.CharField(_('Location'), max_length=512, null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.location


class CompetitionLength(models.Model):
    length = models.DecimalField(_('Length'), decimal_places=1, max_digits=5)
    history = HistoricalRecords()

    def __str__(self):
        leng = int(self.length) if self.length % 1 == 0 else self.length
        if self.length < 50:
            return str(leng) + ' km'
        else:
            return str(leng) + ' m'


class ChampionshipCourseRule(models.Model):
    temp_under4 = models.BooleanField(_('Water temperature under 4 deg'), default=True)
    swimmercategory = models.IntegerField(_('Swimmer category'), choices=SwimmerCategory.choices, default=SwimmerCategory.male_a)
    length = models.ForeignKey(CompetitionLength, on_delete=models.CASCADE, verbose_name=_('Course length'))
    history = HistoricalRecords()


class Competition(models.Model):
    event_date = models.DateField(_('Date'), default=date.today)
    event_venue = models.ForeignKey(CompetitionVenue, on_delete=models.CASCADE, verbose_name=_('Venue'))
    name = models.CharField(_('Name'), max_length=200, default='')
    lengths = models.ManyToManyField(CompetitionLength, verbose_name=_('Lengths'))
    signing_from = models.DateField(_('Sign in from'), default=date.today)
    signing_until = models.DateField(_('Sign in until'), default=date.today)
    expected_temp = models.CharField(_('Expected temperature'), max_length=3,
                                     choices=models.TextChoices('Temperature', '4- 4+ 8+').choices,
                                     default='4-')
    propositions = models.CharField(_('Propositions'), max_length=256, blank=True, null=True)
    iscsps_id = models.IntegerField('IS-CSPS ID')
    is_winter_competition = models.BooleanField(_('Is winter competition'), default=False)
    is_championship_venue = models.BooleanField(_('Is championship'), default=False)
    note = models.CharField(_('Note'), max_length=500, blank=True, null=True)
    copayment = models.IntegerField(_('Copayment'), default=0)
    transportation_payment = models.IntegerField(_('Transportation payment'), default=0)
    transportation_done = models.BooleanField(_('Transportation done'), default=False)
    transportation_locked = models.BooleanField(_('Transportation locked'), default=False)
    history = HistoricalRecords()

    def __str__(self):
        return ' - '.join([str(self.event_venue), str(self.event_date.strftime('%d.%m.%Y'))])


class Person(models.Model):
    class Levels(models.IntegerChoices):
        O = 0, _('0')
        I = 1, _('I')
        II = 2, _('II')
        III = 3, _('III')
        M = 4, _('M')

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, verbose_name=_('User'))
    first_name = models.CharField(_('First name'), max_length=25, default='')
    last_name = models.CharField(_('Last name'), max_length=25, default='')
    email = models.EmailField(max_length=100, default="", blank=True, null=True)
    birth_date = models.DateField(_('Birth date'), default=date.today, help_text="YYYY-MM-DD")
    RC = models.CharField(_('RC'), max_length=18, blank=True, null=True, help_text="YYMMDD/XXXX")
    sex = models.CharField(_('Sex'), max_length=10, choices=models.TextChoices('Sex', 'Male Female').choices,
                           default='Male')

    # contact info
    address = models.CharField(_('Address'), max_length=200, blank=True, null=True)
    phone_number = models.CharField(_('Phone number'), max_length=17, blank=True, null=True)

    # swimming info
    is_honorable = models.BooleanField(_('Is honorable'), default=False)
    member_since = models.DateField(_('Member since'), default=date.today)
    pass_id = models.IntegerField(_('Card ID'), blank=True, null=True)
    points_id = models.IntegerField(_('ID on bodovani.zimni-plavani.info'), default=0)
    iscsps_id = models.IntegerField(_('ID on IS CSPS'), default=0)
    person_allowed_to_register = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True,
                                                   verbose_name=_('Person allowed to register'))
    level_now = models.IntegerField(_('Swimming level actual'), choices=Levels.choices, default=Levels.O)
    level_last_year = models.IntegerField(_('Swimming level previous'), choices=Levels.choices, default=Levels.O)
    hosted_member = models.BooleanField(_('Hosted member'), default=False)
    # aspirant info
    status = models.IntegerField(_('Member status'), choices=MemberStatus.choices, default=MemberStatus.Member)
    temp_registration = models.BooleanField(_('Temporary Registration'), default=False)
    small_reg_form = models.BooleanField(_('Small registration form'), default=False)
    big_reg_form = models.BooleanField(_('Big registration form'), default=False)
    gdpr_agreed = models.BooleanField(_('GDPR Agreed'), default=False)
    information_agreed = models.BooleanField(_('Club Information Signed'), default=False)
    pictures2pcs = models.BooleanField(_('Pictures 2 pcs'), default=False)
    aspirant_note = models.CharField(_('Notes'), max_length=500, blank=True, null=True)
    aspirant_manager = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True,
                                         related_name='Aspirants', verbose_name=_('HR Manager'))
    adr_street = models.CharField(_('Street'), max_length=100, blank=True, null=True)
    adr_home_number_popisne = models.CharField(_('House Number'), max_length=10, blank=True, null=True)
    adr_post_code = models.CharField(_('Post Code'), max_length=10, blank=True, null=True)
    adr_home_number2_orientacni = models.CharField(_('Orientation Number'), max_length=10, blank=True, null=True)
    adr_city = models.CharField(_('City'), max_length=70, blank=True, null=True)
    is_czech_citizenship = models.BooleanField(_('Czech citizenship'), default=True)
    history = HistoricalRecords()

    @property
    def is_aspirant(self):
        return self.status == MemberStatus.Aspirant or self.status == MemberStatus.Accepting

    @property
    def is_escaping(self):
        return self.status == MemberStatus.Escaping

    @property
    def is_treasurer(self):
        return self.user.groups.filter(name='Treasurer Role').exists()

    @property
    def is_competition_manager(self):
        return self.user.groups.filter(name='Competition Manager Role').exists()

    @property
    def is_brigade_manager(self):
        return self.user.groups.filter(name='Brigade Manager').exists()

    @property
    def is_human_resource_manager(self):
        return self.user.groups.filter(name='HR role').exists()

    @property
    def is_medical_exam_manager(self):
        return self.user.groups.filter(name='Medical Examination Role').exists()

    @property
    def get_level_now_pretty(self):
        return self.Levels(self.level_now).label

    @property
    def get_level_last_year_pretty(self):
        return self.Levels(self.level_last_year).label

    def get_swimmerage_type(self, from_year=None):
        if from_year is None:
            from_year = date.today().year
        if self.is_honorable:
            return AgeTypes.elder
        age = from_year - self.birth_date.year
        if age < 18:
            return AgeTypes.junior
        elif age < 70:
            return AgeTypes.regular
        elif age < 80:
            return AgeTypes.senior
        return AgeTypes.elder

    def get_swimmercategory(self):
        age = get_season_end_year(date.today()) - self.birth_date.year
        if age < 20:
            return SwimmerCategory.young_male if self.sex == "Male" else SwimmerCategory.young_female
        elif age < 30:
            return SwimmerCategory.male_a if self.sex == "Male" else SwimmerCategory.female_a
        elif age < 40:
            return SwimmerCategory.male_b if self.sex == "Male" else SwimmerCategory.female_b
        elif age < 50:
            return SwimmerCategory.male_masters_a if self.sex == "Male" else SwimmerCategory.female_masters_a
        elif age < 60:
            return SwimmerCategory.male_masters_b if self.sex == "Male" else SwimmerCategory.female_masters_b
        elif age < 70:
            return SwimmerCategory.male_masters_c if self.sex == "Male" else SwimmerCategory.female_masters_c
        elif age < 80:
            return SwimmerCategory.male_masters_d if self.sex == "Male" else SwimmerCategory.female_masters_d
        return SwimmerCategory.male_masters_e if self.sex == "Male" else SwimmerCategory.female_masters_e

    def update_fees(self, current_date: date, request=None):
        self.update_membership(current_date, request)
        self.update_brigades(current_date, request)

    def update_brigades(self, current_date: date, request=None):
        season_year = get_season_year(current_date)
        if self.member_since > date(season_year, 12, 26):  # check if previous year was member or not
            FulfilledBrigade.objects.update_or_create(person=self,
                                                      year=season_year,
                                                      defaults={
                                                          'fulfilled_brigade': True,
                                                          'missing_brigade_hours': 0,
                                                          'missing_brigade_money': 0})
            FulfilledBrigade.objects.update_or_create(person=self,
                                                      year=season_year - 1,
                                                      defaults={
                                                           'fulfilled_brigade': True,
                                                           'missing_brigade_hours': 0,
                                                           'missing_brigade_money': 0})
        else:
            # UPGRADE BRIGADE INFO
            money_brigade_rate, created = MoneyToBrigadeRate.objects.get_or_create(year=season_year)
            payments = Payment.objects.filter(person=self,
                                              due_date__gte=date(season_year, 7, 1),
                                              due_date__lt=date(season_year + 1, 7, 1))
            brigades = Brigade.objects.filter(person=self,
                                              date__gte=date(season_year, 7, 1),
                                              date__lt=date(season_year + 1, 7, 1),
                                              verified=True)

            if any([b.fulfilled_differently for b in brigades]):
                fulfilled_brigade = True
                missing_brigade_hours = 0
                missing_brigade_money = 0
            else:
                sum_brigades_from_money = sum([p.brigade_amount for p in payments])  # zaplati 200
                sum_brigades_hours_from_money = money_brigade_rate.hours * sum_brigades_from_money / money_brigade_rate.money
                sum_brigades_hours_from_brigades = sum([p.hours for p in brigades])
                sum_brigades_hours_total = sum_brigades_hours_from_brigades + sum_brigades_hours_from_money
                required_brigade_fetch = BrigadeWork.objects.filter(year=season_year,
                                                                    swimmerage_type=self.get_swimmerage_type())
                if len(required_brigade_fetch) > 0:
                    required_brigade = required_brigade_fetch[0].amount
                else:
                    if request:
                        message = "".join([_("There is not defined required brigade hours for"),
                                           " ",
                                           str(self.get_swimmerage_type()),
                                           " ",
                                           _("type of membership in"),
                                           " ",
                                           str(season_year),
                                           ". ",
                                           _("Required brigade setting to 0 hours.")])
                        messages.error(request, message)
                    required_brigade = 0

                fulfilled_brigade = sum_brigades_hours_total >= required_brigade
                missing_brigade_hours = required_brigade - sum_brigades_hours_total
                missing_brigade_money = missing_brigade_hours * money_brigade_rate.money / money_brigade_rate.hours

            FulfilledBrigade.objects.update_or_create(person=self,
                                                      year=season_year,
                                                      defaults={
                                                          'fulfilled_brigade': fulfilled_brigade,
                                                          'missing_brigade_hours': missing_brigade_hours,
                                                          'missing_brigade_money': missing_brigade_money})

    def update_membership(self, current_date: date, request=None):
        if self.member_since.year > current_date.year:  # check if previous year was member or not
            FulfilledMembership.objects.update_or_create(year=current_date.year,
                                                         person=self,
                                                         defaults={
                                                             'fulfilled_membership': True,
                                                             'missing_membership_money': 0
                                                         })
        else:
            # MEMBERSHIP
            required_membership_fees = MembershipFees.objects.filter(year=current_date.year,
                                                                     swimmerage_type=self.get_swimmerage_type())
            if self.member_since.year == current_date.year and self.member_since.month == 12:
                required_membership_fees = MembershipFees.objects.filter(year=current_date.year,
                                                                         swimmerage_type=AgeTypes.elder)
            if self.status == MemberStatus.Aspirant and current_date.year == self.member_since.year and current_date.month == 12:
                required_membership_fees = MembershipFees.objects.filter(year=current_date.year,
                                                                         swimmerage_type=AgeTypes.elder)
            if len(required_membership_fees) > 0:
                required_membership_fee = required_membership_fees[0].amount
            else:
                if request:
                    message = "".join([_("There is not defined fee for"),
                                       " ",
                                       str(self.get_swimmerage_type()),
                                       _("type of membership in"),
                                       str(current_date.year),
                                       ".",
                                       _("Required membership setting to 0 CZK.")])
                    messages.error(request, message)
                required_membership_fee = 0
            payments_first_halfyear = Payment.objects.filter(person=self,
                                                             due_date__gte=date(current_date.year, 1, 1),
                                                             due_date__lte=date(current_date.year, 7, 1))
            payments_all = Payment.objects.filter(person=self,
                                                  due_date__gte=date(current_date.year, 1, 1),
                                                  due_date__lte=date(current_date.year, 12, 31))
            sum_membership_first_halfyear = sum([p.membership_amount for p in payments_first_halfyear])
            sum_membership = sum([p.membership_amount for p in payments_all])
            if self.is_aspirant:
                sum_membership_first_halfyear = sum_membership
            if sum_membership_first_halfyear >= required_membership_fee:
                fulfilled_membership = True
                missing_membership_money = 0
            elif sum_membership >= 2 * required_membership_fee:
                fulfilled_membership = True
                missing_membership_money = 0
            elif sum_membership >= required_membership_fee and self.member_since.year == current_date.year:
                fulfilled_membership = True
                missing_membership_money = 0
            elif payments_all.filter(mf_fulfilled_differently=True).exists():
                fulfilled_membership = True
                missing_membership_money = 0
            else:
                fulfilled_membership = False
                if date.today() < date(current_date.year, 7, 1):
                    missing_membership_money = required_membership_fee - sum_membership
                else:
                    if self.member_since.year == current_date.year and self.member_since.month > 6:
                        missing_membership_money = required_membership_fee - sum_membership
                    else:
                        missing_membership_money = 2 * (required_membership_fee - sum_membership)
            # update or create if needed
            fulfilled_membership_inst, created = FulfilledMembership.objects.update_or_create(year=current_date.year,
                                                                                              person=self,
                                                                                              defaults={
                                                                                                  'fulfilled_membership': fulfilled_membership,
                                                                                                  'missing_membership_money': missing_membership_money
                                                                                              })

    def did_user_pay_membership(self, year=None):
        fulfilled_membership = FulfilledMembership.objects.filter(year=year, person=self).first()
        return fulfilled_membership

    def did_user_pay_brigade(self, season_year=None):
        fulfilled_brigade = FulfilledBrigade.objects.filter(year=season_year, person=self).first()
        return fulfilled_brigade

    def is_eligible_for_registration(self, d):
        return len(self.check_eligibility(d)) == 0

    def check_eligibility(self, d):
        warnings = []
        if self.member_since > d:
            warnings.append(_("You cannot register before you are a member."))

        season_year = get_season_year(d)
        try:
            FulfilledMembership.objects.get(year=season_year, person=self)
        except ObjectDoesNotExist:
            self.update_fees(d)
        try:
            FulfilledMembership.objects.get(year=season_year - 1, person=self)
        except ObjectDoesNotExist:
            self.update_fees(d - relativedelta(years=1))
        try:
            FulfilledMembership.objects.get(year=season_year - 2, person=self)
        except ObjectDoesNotExist:
            self.update_fees(d - relativedelta(years=2))

        eligible_membership_this_year = FulfilledMembership.objects.get(year=season_year,
                                                                        person=self).fulfilled_membership
        if not eligible_membership_this_year:
            warnings.append("".join([_("Membership is not fulfilled in"),
                                     " ",
                                     str(season_year)]))

        eligible_brigade_this_year = self.member_since > date(season_year - 1, 7, 1) \
                                     or FulfilledBrigade.objects.get(year=season_year - 1,
                                                                     person=self).fulfilled_brigade
        if not eligible_brigade_this_year:
            warnings.append("".join([_(
                "Brigade is not fulfilled in season"),
                " ",
                str(season_year - 1),
                "-",
                str(season_year)]))

        eligible_medical_examination = MedicalExamination.objects.filter(person=self,
                                                                         examination_until__gte=d,
                                                                         verified=True).exists()
        if not eligible_medical_examination:
            warning = "".join([_("Medical examination is not valid on"), " ",
                               str(d)])
            warnings.append(warning)

        eligible_membership_last_year = self.member_since.year > d.year - 1 \
                                        or FulfilledMembership.objects.get(year=season_year - 1,
                                                                           person=self).fulfilled_membership
        if not eligible_membership_last_year:
            warnings.append("".join([_("Membership was not fulfilled in"),
                                     " ",
                                     str(d.year - 1)]))

        eligible_brigade_last_year = self.member_since > date(season_year - 2, 7, 1) \
                                     or FulfilledBrigade.objects.get(year=season_year - 2,
                                                                     person=self).fulfilled_brigade
        if not eligible_brigade_last_year:
            warnings.append("".join([_("Brigade was not fulfilled in season"),
                                     " ",
                                     str(season_year - 2),
                                     "-",
                                     str(season_year - 1)]))

        return warnings

    def get_maximal_allowed_competition_lenght(self, competition: Competition):
        if not competition.is_winter_competition:
            return 25
        season_end_year = get_season_end_year(competition.event_date)
        # age of person at the end of the year
        person_age_at_end_season_yearend = season_end_year - self.get_birth_year
        if person_age_at_end_season_yearend < 15:
            return 0
        if person_age_at_end_season_yearend < 18:
            if competition.expected_temp == '8+':
                return 500
            if competition.expected_temp == '4+':
                if self.level_now == 1 or self.level_last_year == 1 or self.level_now == 2 \
                        or self.level_last_year == 2 or self.level_now == 3 or self.level_last_year == 3:
                    return 500
                return 300
            if competition.expected_temp == '4-':
                if self.level_now == 1 or self.level_last_year == 1 or self.level_now == 2 \
                        or self.level_last_year == 2:
                    return 500
                if self.level_now == 3 or self.level_last_year == 3:
                    return 300
            return 100
        # regular age >= 20
        if competition.expected_temp == '8+':
            return 1000
        if self.level_now == 4 or self.level_last_year == 4 or self.level_last_year == 1:
            return 1000
        if self.level_now == 1 or self.level_last_year == 2:
            return 750
        if self.level_last_year == 3 or self.level_now == 2 or self.level_now == 3:
            return 500
        return 300

    def get_swimmerage_type_label(self):
        return AgeTypes(self.get_swimmerage_type()).label

    def get_swimmercategory_label(self):
        return SwimmerCategory(self.get_swimmercategory()).label

    @property
    def get_birth_year(self):
        return self.birth_date.year

    def __str__(self):
        if self.is_aspirant:
            return f"{self.last_name} {self.first_name} ({self.get_birth_year}) (A)"
        elif self.is_escaping:
            return f"{self.last_name} {self.first_name} ({self.get_birth_year}) (x)"
        else:
            return f"{self.last_name} {self.first_name} {self.get_birth_year}"

    def get_public_name(self):
        return f"{self.last_name} {self.first_name}"


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'documents/user_{0}/MedicalExam_{1}_{2}'.format(instance.person.id, instance.examination_until, filename)


class FulfilledMembership(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    year = models.IntegerField(_('Year'), default=2022)  # 1.1.YEAR to 31.12.YEAR

    fulfilled_membership = models.BooleanField(_('Fulfilled'), default=False)
    missing_membership_money = models.IntegerField(_('Needed to pay'), default=0)


class FulfilledBrigade(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    year = models.IntegerField(_('Year'), default=2022)  # 1.7.YEAR to 1.7.YEAR+1

    fulfilled_brigade = models.BooleanField(_('Fulfilled'), default=False)
    missing_brigade_hours = models.FloatField(_('Needed to work'), default=0)
    missing_brigade_money = models.IntegerField(_('Needed to pay'), default=0)


def today_plus_one_year():
    return datetime.today() + relativedelta(years=1)


class MedicalExamination(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    verified = models.BooleanField(_('Verified'), default=False)
    examination_until = models.DateField(_('Valid until'), default=today_plus_one_year)
    examination_file_link = models.FileField(_('Examination file'), upload_to=user_directory_path, null=True,
                                             blank=True,
                                             default=None)
    rejected = models.BooleanField(_('Rejected'), default=False)
    rejected_note = models.CharField(_('Rejected note'), max_length=500, blank=True, null=True)
    history = HistoricalRecords()

    @property
    def is_expired(self):
        return date.today() > self.examination_until

    def __str__(self):
        return str(self.person) + ' ' + str(self.examination_until)


class MembershipFees(models.Model):
    year = models.IntegerField(_('Year'), default=2022)  # valid from 1.7.YEAR to 1.7.YEAR+1
    swimmerage_type = models.IntegerField(_('Swimmer age type'), choices=AgeTypes.choices, default=AgeTypes.regular)
    amount = models.IntegerField(_('Fee'))
    history = HistoricalRecords()

    def __str__(self):
        return ' - '.join(
            [str(self.year), str(get_swimmerage_type(self.swimmerage_type)), str(self.year), str(self.amount)])


class BrigadeWork(models.Model):
    year = models.IntegerField(_('Year'), default=2022)  # valid from 1.7.YEAR to 30.6.YEAR+1
    swimmerage_type = models.IntegerField(_('Membership type'), choices=AgeTypes.choices, default=AgeTypes.regular)
    amount = models.IntegerField(_('Required Hours'))
    history = HistoricalRecords()

    def __str__(self):
        return ' - '.join([str(self.year), str(get_swimmerage_type(self.swimmerage_type)), str(self.amount) + ' CZK'])


class MoneyToBrigadeRate(models.Model):
    year = models.IntegerField(_('Year'), default=2022)  # valid from 1.7.YEAR to 1.7.YEAR+1
    money = models.IntegerField(_('Money'), default=20)
    hours = models.IntegerField(_('Hours'), default=1)
    history = HistoricalRecords()

    def __str__(self):
        return ' - '.join([str(self.year), str(self.money) + ' czk = ' + str(self.hours) + ' hr'])


class Payment(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    due_date = models.DateField(_('Due date'), default=date.today)
    date = models.DateField(_('Payment date'), default=date.today)
    membership_amount = models.IntegerField(_('Membership Fee'), default=0)
    brigade_amount = models.IntegerField(_('Brigade Fee'), default=0)
    way_of_transfer = models.CharField(_('Way of transfer'), max_length=20,
                                       choices=models.TextChoices('Way of transfer', 'cash bank other').choices,
                                       default='bank')
    mf_fulfilled_differently = models.BooleanField(_('MF Fulfilled differently'), default=False)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.date) + ' ' + str(self.person) + ' - ' + str(
            self.membership_amount) + ' CZK + ' + str(self.brigade_amount) + " CZK"


class Brigade(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    date = models.DateField(_('Work date'), default=date.today)
    hours = models.FloatField(_('Hours'), default=0)
    work = models.CharField(_('Work'), default='', max_length=1024, null=True, blank=True)
    accountable = models.ForeignKey(Person,
                                    on_delete=models.CASCADE,
                                    null=True,
                                    blank=True,
                                    related_name='Accountable',
                                    verbose_name=_('Accountable'))
    fulfilled_differently = models.BooleanField(_('Fulfilled differently'), default=False)
    history = HistoricalRecords()
    verified = models.BooleanField(_('Verified'), default=False)

    def __str__(self):
        return str(self.person) + ' - ' + str(self.hours) + ' hours'


class Transport(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE, verbose_name=_('Competition'))
    driver = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Driver'))
    passengers = models.ManyToManyField(Person, related_name='Passengers', verbose_name=_('Passengers'))
    note = models.TextField(_('Note'), blank=True, null=True, default="")
    available_seats = models.IntegerField(_('Available seats'), default=0)


class Registration(models.Model):
    class Transports(models.IntegerChoices):
        SOLO = 0, _('S')  # Samostatně
        PASSENGER = 1, _('AX')  # Chci se svést
        DRIVER1 = 2, _('AŘ+1')  # Vezmu auto - nabízím 1 další volné místo
        DRIVER2 = 3, _('AŘ+2')  # Vezmu auto - nabízím 2 další volná místa
        DRIVER3 = 4, _('AŘ+3')  # Vezmu auto - nabízím 3 další volná místa
        DRIVER4 = 5, _('AŘ+4')  # Vezmu auto - nabízím 4 další volná místa
        DRIVER5 = 6, _('AŘ+5')  # Vezmu auto - nabízím 5 dalších volných míst
        DRIVER6 = 7, _('AŘ+6')  # Vezmu auto - nabízím 6 dalších volných míst

    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name=_('Person'))
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE, verbose_name=_('Competition'))
    championship_course = models.BooleanField(default=False)
    length = models.ManyToManyField(CompetitionLength, verbose_name=_('Length'))
    transport = models.IntegerField(_('Transport'), choices=Transports.choices)
    note = models.TextField(_('Note'), blank=True, null=True, default="")
    solution = models.TextField(_('Solution'), blank=True, null=True, default="")
    confirmed_in_IS = models.BooleanField(default=False)
    co_paid = models.BooleanField(default=False)
    history = HistoricalRecords()

    preferred_driver = models.ForeignKey(Person, related_name='preferred_driver', null=True,  blank=True, on_delete=models.CASCADE, verbose_name=_('Preferred driver'))
    preferred_passengers = models.ManyToManyField(Person, related_name='preferred_passengers', blank=True, verbose_name=_('Preferred Passengers'))

    @property
    def get_transport_pretty(self):
        return self.Transports(self.transport).label.upper()

    def __str__(self):
        return str(self.competition) + ' - ' + str(self.person)

    def get_track_limit_8(self):
        exptemp = self.competition.expected_temp
        self.competition.expected_temp = "4+"
        limit = self.person.get_maximal_allowed_competition_lenght(self.competition)
        self.competition.expected_temp = exptemp
        return limit

    def get_track_limit_4(self):
        exptemp = self.competition.expected_temp
        self.competition.expected_temp = "4-"
        limit = self.person.get_maximal_allowed_competition_lenght(self.competition)
        self.competition.expected_temp = exptemp
        return limit


class InternalLog(models.Model):
    category = models.TextField(_('Category'), blank=True, null=True, default="")
    log_entry = models.TextField(_('Log'), blank=True, null=True, default="")
    result = models.TextField(_('Result'), blank=True, null=True, default="")
    time_stamp = models.DateTimeField(_('Timestamp'), auto_now_add=True)
