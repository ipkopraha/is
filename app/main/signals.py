from contextlib import contextmanager
from datetime import date
from functools import wraps
from threading import local

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import Group
from django.core.mail import send_mail
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from .models import Brigade, Payment, Person, MedicalExamination, Registration
from .utils import apply_for_competition
from .views import get_current_person

MUTE_SIGNALS_ATTR = '_mute_signals'

_request_local = local()


class CaptureRequestMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        _request_local.current_request = request
        response = self.get_response(request)
        return response


def mutable_signal_receiver(func):
    """Decorator for signals to allow to skip them by setting attr MUTE_SIGNALS_ATTR on instance,
    which can be done via mute_signals_for"""

    @wraps(func)
    def wrapper(sender, signal, **kwargs):
        if 'instance' in kwargs:
            instance = kwargs['instance']
            mute_signals = getattr(instance, MUTE_SIGNALS_ATTR, False)
            if mute_signals is True:
                pass  # skip all signals
            elif isinstance(mute_signals, list) and signal in mute_signals:
                pass  # skip user requested signal
            else:  # allow signal receiver
                return func(sender=sender, signal=signal, **kwargs)

    return wrapper


def request_exists(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not hasattr(_request_local, 'current_request'):
            return None
        return func(*args, **kwargs)

    return wrapper


@contextmanager
def mute_signals_for(instance, signals):
    """Context manager to skip signals for @instance (django model), @signals can be
    True to skip all signals or list of specified signals, like [post_delete, post_save] """
    try:
        yield setattr(instance, MUTE_SIGNALS_ATTR, signals)
    finally:
        setattr(instance, MUTE_SIGNALS_ATTR, False)


@receiver([post_save, post_delete], sender=Brigade)
@mutable_signal_receiver
@request_exists
def brigade_change(sender, **kwargs):
    if 'instance' in kwargs:
        person_payed = kwargs['instance'].person
        date = kwargs['instance'].date

        group = Group.objects.filter(name='Brigade Manager').first()
        if group is not None:
            recipient_list = [kwargs['instance'].accountable.email]
            print(recipient_list)
        else:
            recipient_list = []

        url = reverse('main:brigade_requests')  # Adjust the URL name based on your app and view configuration
        request = getattr(_request_local, 'current_request', None)
        if request:
            protocol = 'https' if request.is_secure() else 'http'
            domain = request.get_host()
        else:
            protocol = 'https'
        link = f"{protocol}://{domain}{url}"

        subject = 'IS I. PKO - nová brigáda ke schválení.'
        message = 'Ahoj,\n' + str(person_payed) + ' právě nahlásil brigádu. Děkujeme za její schválení na ' + link + '\n' + 'S pozdravem, IS I. PKO'
        email_from = settings.EMAIL_HOST_USER
        send_mail(subject, message, email_from, recipient_list)

        person_payed.update_fees(date)
        messages.info(request, _("Notification email sent to") + f" {recipient_list}")


@receiver([post_save, post_delete], sender=Payment)
@mutable_signal_receiver
@request_exists
def payment_change(sender, **kwargs):
    if 'instance' in kwargs:
        person_payed = kwargs['instance'].person
        due_date = kwargs['instance'].due_date

        person_payed.update_fees(due_date)


@receiver(post_save, sender=Person)
@mutable_signal_receiver
@request_exists
def new_person(sender, **kwargs):
    if 'instance' in kwargs:
        person = kwargs['instance']
        today = date.today()

        person.update_fees(today)

        years_ago = today - relativedelta(years=1)
        person.update_fees(years_ago)


@receiver(post_save, sender=MedicalExamination)
@mutable_signal_receiver
@request_exists
def new_medical_exam(sender, **kwargs):
    if 'instance' in kwargs:
        # get all Medical Examination Managers
        group = Group.objects.filter(name='Medical Examination Role').first()
        if group is not None:
            all_recipients = group.user_set.filter(is_superuser=False)
            recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
        else:
            recipient_list = []

        url = reverse('main:medical_exams')  # Adjust the URL name based on your app and view configuration
        request = getattr(_request_local, 'current_request', None)
        if request:
            protocol = 'https' if request.is_secure() else 'http'
            domain = request.get_host()
            link = f"{protocol}://{domain}{url}"
        else:
            link = ''
        examination = kwargs['instance']
        exam_person = examination.person
        if request != None:
            current_person = get_current_person(request)
            if exam_person == current_person:
                subject = 'Nová zdravotní prohlídka čeká na zpracování.'
                message = ('Ahoj,' + '\n' + str(exam_person) + ' ' + 'právě nahrál/a novou lékařskou prohlídku.' + '\n' +
                           'Děkujeme za ověření prohlídky na ' + link + '\n' + 'S pozdravem, IS I. PKO')
                email_from = settings.EMAIL_HOST_USER
                send_mail(subject, message, email_from, recipient_list)

        return redirect('main:user_info')

