import datetime

from django.test import TestCase

from main.forms import RegistrationForm
from main.models import CompetitionVenue, CompetitionLength, Competition, Person


class RegistrationFormTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        venue1 = CompetitionVenue.objects.create(location='Komorany')

        lengths = [CompetitionLength.objects.create(length=i) for i in [100, 250, 300, 500, 750, 1000]]
        com10 = Competition.objects.create(name='Jicin 10',
                                           event_venue=venue1,
                                           event_date=datetime.date(year=2020,
                                                                    month=10,
                                                                    day=10),
                                           expected_temp='8+',
                                           is_winter_competition=True,
                                           iscsps_id=1000
                                           )
        [com10.lengths.add(i) for i in lengths]
        com10.save()

        com2 = Competition.objects.create(name='Jicin 2',
                                          event_venue=venue1,
                                          expected_temp='4-',
                                          is_winter_competition=True,
                                          iscsps_id=1000,
                                          event_date=datetime.date(year=2020,
                                                                   month=10,
                                                                   day=10),
                                          )
        com2.save()
        [com2.lengths.add(i) for i in lengths]

    def test_junior(self):
        abel = Person.objects.create(first_name='Abel',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        abel_max = abel.get_maximal_allowed_competition_lenght(competition10)
        form = RegistrationForm(person=abel, competition=competition10)
        self.assertEqual(len(form.fields['length'].queryset), 4)
        for l in form.fields['length'].queryset:
            self.assertLessEqual(l.length, abel_max)

    def test_regular(self):
        abel = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(2000, 1, 1),
                                     level_now=0,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        abel_max = abel.get_maximal_allowed_competition_lenght(competition10)
        form = RegistrationForm(person=abel, competition=competition10)
        self.assertEqual(len(form.fields['length'].queryset), 6)
        for l in form.fields['length'].queryset:
            self.assertLessEqual(l.length, abel_max)
