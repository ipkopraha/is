import datetime

from django.test import TestCase

from main.models import Person, Payment, MembershipFees, \
    AgeTypes, FulfilledMembership, BrigadeWork, MoneyToBrigadeRate, \
    FulfilledBrigade, Brigade
import time_machine


@time_machine.travel(datetime.datetime(2022, 10, 1))
class PaymentTestsRegularAutumn(TestCase):

    def setUp(self):

        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1980,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )

    def test_payments_and_brigades_newbie_fall(self):
        kain, _ = Person.objects.get_or_create(first_name='Kain',
                                               sex='Male',
                                               birth_date=datetime.date(year=2000,
                                                                        month=1,
                                                                        day=1),
                                               member_since=datetime.date(year=2022,
                                                                          month=3,
                                                                          day=1),
                                               )

        assert len(FulfilledMembership.objects.all()) == 4  # 2022 and 2021 should be added
        assert FulfilledMembership.objects.get(year=2021, person=kain).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2022, person=kain).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2022, person=kain).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021, person=kain).fulfilled_brigade is True

    def test_payments_membership(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledMembership.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledMembership.objects.all():
            assert item.fulfilled_membership is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=500,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is True
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

    def test_payments_membership_delay(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=9, day=1),
                                         date=datetime.date(year=2022, month=9, day=1),
                                         membership_amount=400,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment2 = Payment.objects.create(person=adam,
                                          due_date=datetime.date(year=2022, month=9, day=1),
                                          date=datetime.date(year=2022, month=9, day=1),
                                          membership_amount=600,
                                          brigade_amount=0,
                                          way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is True
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2022).first().missing_membership_money == 400

    def test_payments_membership_delay_last_year(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2021, month=9, day=1),
                                         date=datetime.date(year=2021, month=9, day=1),
                                         membership_amount=400,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment2 = Payment.objects.create(person=adam,
                                          due_date=datetime.date(year=2021, month=9, day=1),
                                          date=datetime.date(year=2021, month=9, day=1),
                                          membership_amount=600,
                                          brigade_amount=0,
                                          way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is True

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().missing_membership_money == 400

    def test_payments_membership_previous_year(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2021, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=500,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is True

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

@time_machine.travel(datetime.datetime(2022, 3, 1))
class PaymentTestsRegularSping(TestCase):

    def setUp(self):

        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1980,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )
    def test_next_year_brigade(self):
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=8, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=0,
                                         brigade_amount=6000,
                                         way_of_transfer='cash')

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True, [a.__dict__ for a in FulfilledBrigade.objects.all()]
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False

        payment.delete()

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False, [a.__dict__ for a in FulfilledBrigade.objects.all()]
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False



    def test_payments_and_brigades_newbie(self):
        kain, _ = Person.objects.get_or_create(first_name='Kain',
                                               sex='Male',
                                               birth_date=datetime.date(year=2000,
                                                                        month=1,
                                                                        day=1),
                                               member_since=datetime.date(year=2022,
                                                                          month=2,
                                                                          day=1),
                                               )

        assert len(FulfilledMembership.objects.all()) == 4  # 2022 and 2021 should be added
        assert FulfilledMembership.objects.get(year=2021, person=kain).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2022, person=kain).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020, person=kain).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021, person=kain).fulfilled_brigade is True

    def test_payments_membership(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledMembership.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledMembership.objects.all():
            assert item.fulfilled_membership is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=500,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is True
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

    def test_payments_membership_delay_last_year(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2021, month=9, day=1),
                                         date=datetime.date(year=2021, month=9, day=1),
                                         membership_amount=400,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment2 = Payment.objects.create(person=adam,
                                          due_date=datetime.date(year=2021, month=9, day=1),
                                          date=datetime.date(year=2021, month=9, day=1),
                                          membership_amount=600,
                                          brigade_amount=0,
                                          way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is True

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().missing_membership_money == 400

    def test_payments_membership_previous_year(self):
        """ Test if payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2021, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=500,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is True

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

@time_machine.travel(datetime.datetime(2022, 10, 1))
class PaymentTestsSeniorFall(TestCase):
    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=300)
        MoneyToBrigadeRate.objects.create(year=2022,
                                          money=50,
                                          hours=1)
        MoneyToBrigadeRate.objects.create(year=2021,
                                          money=50,
                                          hours=1)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1952,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )

    def test_payments_membership_fall(self):
        """ Test if payments for brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledMembership.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledMembership.objects.all():
            assert item.fulfilled_membership is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=250,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is True
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False

    def test_payments_brigades(self):
        """ Test if brigade payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledBrigade.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledBrigade.objects.all():
            assert item.fulfilled_brigade is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=0,
                                         brigade_amount=50,
                                         way_of_transfer='cash')

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False
        assert FulfilledBrigade.objects.filter(year=2021).first().missing_brigade_hours == 199
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False

        payment.delete()

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False
        assert FulfilledBrigade.objects.filter(year=2021).first().missing_brigade_hours == 200
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False

        payment2 = Payment.objects.create(person=adam,
                                          due_date=datetime.date(year=2022, month=2, day=1),
                                          date=datetime.date(year=2022, month=2, day=1),
                                          membership_amount=0,
                                          brigade_amount=10000,
                                          way_of_transfer='cash')
        assert len(FulfilledBrigade.objects.filter(year=2021)) == 1
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is True

        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=8, day=1),
                               date=datetime.date(year=2022, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=15000,
                               way_of_transfer='cash')
        assert len(FulfilledBrigade.objects.filter(year=2022)) == 1
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades_not_verified(self):
        """ Test if brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        brigade = Brigade.objects.create(person=adam,
                                         date=datetime.date(year=2022, month=8, day=1),
                                         hours=300,
                                         )
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False

        brigade.verified = True
        brigade.save()

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades_verified(self):
        """ Test if brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2022, month=8, day=1),
                               hours=300,
                               verified=True)

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades(self):
        adam = Person.objects.get(first_name="Adam")

        brigade = Brigade.objects.create(person=adam,
                                         date=datetime.date(year=2022, month=8, day=1),
                                         hours=200,
                                         verified=True)
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False

        brigade2 = Brigade.objects.create(person=adam,
                                          date=datetime.date(year=2022, month=8, day=1),
                                          hours=100,
                                          verified=True)
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

        brigade2.delete()
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=8, day=1),
                               date=datetime.date(year=2022, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=5000,
                               way_of_transfer='cash')
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

@time_machine.travel(datetime.datetime(2022, 1, 10))
class PaymentTestsSeniorSpring(TestCase):
    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.senior,
                                      amount=250,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.senior,
                                   amount=300)
        MoneyToBrigadeRate.objects.create(year=2022,
                                          money=50,
                                          hours=1)
        MoneyToBrigadeRate.objects.create(year=2021,
                                          money=50,
                                          hours=1)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1952,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )

    def test_payments_membership_fall(self):
        """ Test if payments for brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledMembership.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledMembership.objects.all():
            assert item.fulfilled_membership is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=250,
                                         brigade_amount=0,
                                         way_of_transfer='cash')

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is True
        assert FulfilledMembership.objects.filter(year=2021).first().fulfilled_membership is False

        payment.delete()

        assert FulfilledMembership.objects.filter(year=2022).first().fulfilled_membership is False

    def test_payments_brigades(self):
        """ Test if brigade payments are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        assert len(FulfilledBrigade.objects.all()) == 2  # 2022 and 2021 should be added
        for item in FulfilledBrigade.objects.all():
            assert item.fulfilled_brigade is False

        payment = Payment.objects.create(person=adam,
                                         due_date=datetime.date(year=2022, month=2, day=1),
                                         date=datetime.date(year=2022, month=2, day=1),
                                         membership_amount=0,
                                         brigade_amount=50,
                                         way_of_transfer='cash')

        assert FulfilledBrigade.objects.filter(year=2020).first().fulfilled_brigade is False
        assert FulfilledBrigade.objects.filter(year=2021).first().missing_brigade_hours == 199
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False

        payment.delete()

        assert FulfilledBrigade.objects.filter(year=2020).first().fulfilled_brigade is False
        assert FulfilledBrigade.objects.filter(year=2021).first().missing_brigade_hours == 200
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is False

        payment2 = Payment.objects.create(person=adam,
                                          due_date=datetime.date(year=2022, month=2, day=1),
                                          date=datetime.date(year=2022, month=2, day=1),
                                          membership_amount=0,
                                          brigade_amount=10000,
                                          way_of_transfer='cash')
        assert len(FulfilledBrigade.objects.filter(year=2021)) == 1
        assert FulfilledBrigade.objects.filter(year=2021).first().fulfilled_brigade is True

        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=8, day=1),
                               date=datetime.date(year=2022, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=15000,
                               way_of_transfer='cash')
        assert len(FulfilledBrigade.objects.filter(year=2022)) == 1
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades_not_verified(self):
        """ Test if brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        brigade = Brigade.objects.create(person=adam,
                                         date=datetime.date(year=2022, month=8, day=1),
                                         hours=300,
                                         )
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False

        brigade.verified = True
        brigade.save()

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades_verified(self):
        """ Test if brigade are handled in correct years """
        adam = Person.objects.get(first_name="Adam")

        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2022, month=8, day=1),
                               hours=300,
                               verified=True)

        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

    def test_brigades(self):
        adam = Person.objects.get(first_name="Adam")

        brigade = Brigade.objects.create(person=adam,
                                         date=datetime.date(year=2022, month=8, day=1),
                                         hours=200,
                                         verified=True)
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False

        brigade2 = Brigade.objects.create(person=adam,
                                          date=datetime.date(year=2022, month=8, day=1),
                                          hours=100,
                                          verified=True)
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True

        brigade2.delete()
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is False
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=8, day=1),
                               date=datetime.date(year=2022, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=5000,
                               way_of_transfer='cash')
        assert FulfilledBrigade.objects.filter(year=2022).first().fulfilled_brigade is True
