import datetime

from django.test import TestCase

from main.models import Person, Payment, MembershipFees, Brigade, \
    AgeTypes, FulfilledMembership, BrigadeWork, FulfilledBrigade, MedicalExamination
import time_machine


@time_machine.travel(datetime.datetime(2022, 10, 1))
class PaymentTestsRegularFall(TestCase):

    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1981,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )

    def test_nothing_fulfilled(self):
        adam = Person.objects.get(first_name="Adam")
        assert len(FulfilledMembership.objects.all()) == 2
        assert len(FulfilledBrigade.objects.all()) == 2
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 2)) is False

    def test_payment_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=2, day=1),
                               date=datetime.date(year=2022, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False

    def test_payment_fall_2022_due_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=3, day=1),
                               date=datetime.date(year=2022, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False

    def test_payment_fall_2021(self):
        adam = Person.objects.get(first_name="Adam")
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False

    def test_eligible_for_registration_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2020, month=2, day=1),
                               date=datetime.date(year=2020, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=1000,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021 - brigade
                               due_date=datetime.date(year=2021, month=3, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False

        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is True

    def test_eligible_for_registration_fall_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=2, day=1),
                               date=datetime.date(year=2022, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021
                               due_date=datetime.date(year=2021, month=3, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2020, month=8, day=1),
                               hours=200,
                               verified=True)
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2021, month=8, day=1),
                               hours=200,
                               verified=True)
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is True


@time_machine.travel(datetime.datetime(2022, 3, 1))
class PaymentTestsRegularSprint(TestCase):

    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2019,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2019,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1981,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2000,
                                                         month=1,
                                                         day=1),
                              )

    def test_nothing_fulfilled(self):
        adam = Person.objects.get(first_name="Adam")
        assert len(FulfilledMembership.objects.all()) == 2
        assert len(FulfilledBrigade.objects.all()) == 2
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 2)) is False

    def test_payment_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=2, day=1),
                               date=datetime.date(year=2022, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True

    def test_payment_fall_2021(self):
        adam = Person.objects.get(first_name="Adam")
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False

    def test_eligible_for_registration_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2020, month=2, day=1),
                               date=datetime.date(year=2020, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021
                               due_date=datetime.date(year=2021, month=8, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=1000,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021 - brigade
                               due_date=datetime.date(year=2021, month=3, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=0,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False

        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is True

    def test_eligible_for_registration_fall_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=2, day=1),
                               date=datetime.date(year=2022, month=2, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Payment.objects.create(person=adam,  # late payment spring 2021
                               due_date=datetime.date(year=2021, month=3, day=1),
                               date=datetime.date(year=2021, month=8, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2020, month=8, day=1),
                               hours=200,
                               verified=True)
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        Brigade.objects.create(person=adam,
                               date=datetime.date(year=2021, month=8, day=1),
                               hours=200,
                               verified=True)
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is False
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is True


@time_machine.travel(datetime.datetime(2022, 11, 1))
class EligibleTestsNewbieAutumn(TestCase):
    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1981,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2022,
                                                         month=10,
                                                         day=10),
                              )

    def test_eligible_for_registration_fall_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 12, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=6, day=1),
                               date=datetime.date(year=2022, month=11, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 12, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 12, 1)) is True

    def test_eligible_for_registration_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=6, day=1),
                               date=datetime.date(year=2022, month=11, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # cannot register before is member


@time_machine.travel(datetime.datetime(2023, 3, 1))
class EligibleTestsNewbieSpring(TestCase):
    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1981,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2022,
                                                         month=3,
                                                         day=1),
                              )

    def test_eligible_for_registration_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 10)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=3, day=1),
                               date=datetime.date(year=2022, month=3, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 10)) is False  # payment spring 2020
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 10)) is True

    def test_eligible_for_registration_autumn_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=3, day=1),
                               date=datetime.date(year=2022, month=3, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 8, 1)) is True


@time_machine.travel(datetime.datetime(2023, 3, 1))
class EligibleTestsLastYearNewbieAutumn(TestCase):
    def setUp(self):
        MembershipFees.objects.create(year=2022,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2021,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        MembershipFees.objects.create(year=2020,
                                      swimmerage_type=AgeTypes.regular,
                                      amount=500,
                                      )
        BrigadeWork.objects.create(year=2020,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2021,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=200)
        BrigadeWork.objects.create(year=2022,
                                   swimmerage_type=AgeTypes.regular,
                                   amount=300)
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=1981,
                                                       month=1,
                                                       day=1),
                              member_since=datetime.date(year=2021,
                                                         month=10,
                                                         day=10),
                              )

    def test_eligible_for_registration_fall_2021(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2021, 12, 1)) is False  # missing all
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2021, 12, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=6, day=1),
                               date=datetime.date(year=2021, month=11, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2021, 12, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2021, 12, 1)) is True

    def test_eligible_for_registration_spring_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=6, day=1),
                               date=datetime.date(year=2021, month=11, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2019).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is False  # payment spring 2020
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 3, 1)) is True

    def test_eligible_for_registration_fall_2022(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.is_eligible_for_registration(datetime.date(2022, 9, 1)) is False  # missing all
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2021, month=6, day=1),
                               date=datetime.date(year=2021, month=11, day=1),
                               membership_amount=500,
                               brigade_amount=0,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is False
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is False
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 9, 1)) is False
        MedicalExamination.objects.create(person=adam,
                                          verified=True,
                                          examination_until=datetime.date(2023, 1, 1))
        assert adam.is_eligible_for_registration(datetime.date(2022, 9, 1)) is False
        Payment.objects.create(person=adam,
                               due_date=datetime.date(year=2022, month=6, day=1),
                               date=datetime.date(year=2022, month=6, day=1),
                               membership_amount=500,
                               brigade_amount=4000,
                               way_of_transfer='cash')
        assert FulfilledMembership.objects.get(year=2022).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2021).fulfilled_membership is True
        assert FulfilledMembership.objects.get(year=2020).fulfilled_membership is True
        assert FulfilledBrigade.objects.get(year=2020).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2021).fulfilled_brigade is True
        assert FulfilledBrigade.objects.get(year=2022).fulfilled_brigade is False
        assert adam.is_eligible_for_registration(datetime.date(2022, 9, 1)) is True
