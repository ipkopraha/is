import datetime

from django.test import TestCase

from main.models import Person, Competition, CompetitionVenue, CompetitionLength, Registration


class RegistrationTestCase(TestCase):
    def setUp(self):
        Person.objects.create(first_name='Adam',
                              sex='Male')
        Person.objects.create(first_name='Alice',
                              sex='Female')
        CompetitionVenue.objects.create(location='Prague')
        length = CompetitionLength.objects.create(length=250)
        competition = Competition.objects.create(
            event_date=datetime.date.today(),
            event_venue=CompetitionVenue.objects.filter()[:1].get(),
            iscsps_id=1234
        )
        competition.lengths.add(length)

    def test_persons(self):
        """Persons have correct sex"""
        adam = Person.objects.get(first_name="Adam")
        alice = Person.objects.get(first_name="Alice")
        competition = Competition.objects.filter()[:1].get()
        length = CompetitionLength.objects.filter()[:1].get()

        registration = Registration.objects.create(
            person=adam,
            competition=competition,
        )
        registration.length.add(length)
        self.assertEqual(adam.sex, 'Male')
        self.assertEqual(alice.sex, 'Female')
