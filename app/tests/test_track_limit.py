import datetime

from django.test import TestCase

from main.forms import RegistrationForm
from main.models import Person, Competition, CompetitionVenue, CompetitionLength


class TestTrackLimits(TestCase):

    def form(self, person, competition, track_limit):
        form = RegistrationForm(person=person, competition=competition)
        lengths = competition.lengths.filter(length__lte=track_limit)
        self.assertEqual(len(form.fields['length'].queryset), len(lengths))
        for l in form.fields['length'].queryset:
            self.assertLessEqual(l.length, track_limit)

    def setUp(self):
        venue1 = CompetitionVenue.objects.create(location='Komorany')
        lengths = [CompetitionLength.objects.create(length=i) for i in [100, 250, 300, 500, 750, 1000]]
        com10 = Competition.objects.create(name='Jicin 10',
                                           event_venue=venue1,
                                           event_date=datetime.date(year=2020,
                                                                    month=10,
                                                                    day=10),
                                           expected_temp='8+',
                                           is_winter_competition=True,
                                           iscsps_id=1000
                                           )
        com10.save()
        [com10.lengths.add(i) for i in lengths]
        com6 = Competition.objects.create(name='Jicin 6',
                                          event_venue=venue1,
                                          expected_temp='4+',
                                          is_winter_competition=True,
                                          iscsps_id=1000,
                                          event_date=datetime.date(year=2020,
                                                                   month=10,
                                                                   day=10),
                                          )
        com6.save()
        [com6.lengths.add(i) for i in lengths]
        com2 = Competition.objects.create(name='Jicin 2',
                                          event_venue=venue1,
                                          expected_temp='4-',
                                          is_winter_competition=True,
                                          iscsps_id=1000,
                                          event_date=datetime.date(year=2020,
                                                                   month=10,
                                                                   day=10),
                                          )
        com2.save()
        [com2.lengths.add(i) for i in lengths]
        comS = Competition.objects.create(name='Jicin leto',
                                          event_venue=venue1,
                                          expected_temp='8+',
                                          is_winter_competition=False,
                                          iscsps_id=1000,
                                          event_date=datetime.date(year=2020,
                                                                   month=8,
                                                                   day=10),
                                          )
        comS.save()
        [comS.lengths.add(i) for i in lengths]

    def test_swimming_level_0_pupil(self):
        abel = Person.objects.create(first_name='Abel',
                                     sex='Male',
                                     birth_date=datetime.date(year=2007,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert abel.get_maximal_allowed_competition_lenght(competition10) == 0
        self.form(abel, competition10, 0)

        competition6 = Competition.objects.get(name='Jicin 6')
        assert abel.get_maximal_allowed_competition_lenght(competition6) == 0
        self.form(abel, competition6, 0)

        competition2 = Competition.objects.get(name='Jicin 2')
        assert abel.get_maximal_allowed_competition_lenght(competition2) == 0
        self.form(abel, competition2, 0)

        competitionS = Competition.objects.get(name='Jicin leto')
        assert abel.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(abel, competitionS, 0)

    def test_swimming_level_0_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)

        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 300
        self.form(kain, competition6, 300)

        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 100
        self.form(kain, competition2, 100)

        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_0_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=0,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)

        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 300
        self.form(adam, competition6, 300)

        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 300
        self.form(adam, competition2, 300)

        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_III_now_pupil(self):
        abel = Person.objects.create(first_name='Abel',
                                     sex='Male',
                                     birth_date=datetime.date(year=2007,
                                                              month=1,
                                                              day=1),

                                     level_now=3,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert abel.get_maximal_allowed_competition_lenght(competition10) == 0
        self.form(abel, competition10, 0)

        competition6 = Competition.objects.get(name='Jicin 6')
        assert abel.get_maximal_allowed_competition_lenght(competition6) == 0
        self.form(abel, competition6, 0)

        competition2 = Competition.objects.get(name='Jicin 2')
        assert abel.get_maximal_allowed_competition_lenght(competition2) == 0
        self.form(abel, competition2, 0)

        competitionS = Competition.objects.get(name='Jicin leto')
        assert abel.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(abel, competitionS, 25)

    def test_swimming_level_III_now_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=3,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 300
        self.form(kain, competition2, 300)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_III_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=3,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(adam, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(adam, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_II_now_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=2,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(kain, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_II_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=2,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(adam, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(adam, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_I_now_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=1,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(kain, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_I_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=1,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10,1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 750
        self.form(adam, competition6, 750)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 750
        self.form(adam, competition2, 750)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_M_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=4,
                                     level_last_year=0
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 1000
        self.form(adam, competition6, 1000)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 1000
        self.form(adam, competition2, 1000)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_III_last_pupil(self):
        abel = Person.objects.create(first_name='Abel',
                                     sex='Male',
                                     birth_date=datetime.date(year=2007,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=2
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert abel.get_maximal_allowed_competition_lenght(competition10) == 0
        self.form(abel, competition10, 0)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert abel.get_maximal_allowed_competition_lenght(competition6) == 0
        self.form(abel, competition6, 0)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert abel.get_maximal_allowed_competition_lenght(competition2) == 0
        self.form(abel, competition2, 0)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert abel.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(abel, competitionS, 25)

    def test_swimming_level_III_last_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=3
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 300
        self.form(kain, competition2, 300)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_III_last_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=0,
                                     level_last_year=3
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(adam, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(adam, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_II_last_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=2
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(kain, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_II_last_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=0,
                                     level_last_year=2
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 750
        self.form(adam, competition6, 750)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 750
        self.form(adam, competition2, 750)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_I_last_junior(self):
        kain = Person.objects.create(first_name='Kain',
                                     sex='Male',
                                     birth_date=datetime.date(year=2006,
                                                              month=1,
                                                              day=1),

                                     level_now=0,
                                     level_last_year=1
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert kain.get_maximal_allowed_competition_lenght(competition10) == 500
        self.form(kain, competition10, 500)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert kain.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(kain, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert kain.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(kain, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert kain.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(kain, competitionS, 25)

    def test_swimming_level_I_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=0,
                                     level_last_year=1
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 1000
        self.form(adam, competition6, 1000)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 1000
        self.form(adam, competition2, 1000)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_M_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=0,
                                     level_last_year=4
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 1000
        self.form(adam, competition6, 1000)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 1000
        self.form(adam, competition2, 1000)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_M_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=1,
                                     level_last_year=2
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 750
        self.form(adam, competition6, 750)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 750
        self.form(adam, competition2, 750)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)

    def test_swimming_level_M_now_regular(self):
        adam = Person.objects.create(first_name='Adam',
                                     sex='Male',
                                     birth_date=datetime.date(year=2001,
                                                              month=12,
                                                              day=31),
                                     member_since=datetime.date(year=2000,
                                                                month=1,
                                                                day=1),
                                     level_now=2,
                                     level_last_year=3
                                     )
        competition10 = Competition.objects.get(name='Jicin 10')
        assert adam.get_maximal_allowed_competition_lenght(competition10) == 1000
        self.form(adam, competition10, 1000)
        competition6 = Competition.objects.get(name='Jicin 6')
        assert adam.get_maximal_allowed_competition_lenght(competition6) == 500
        self.form(adam, competition6, 500)
        competition2 = Competition.objects.get(name='Jicin 2')
        assert adam.get_maximal_allowed_competition_lenght(competition2) == 500
        self.form(adam, competition2, 500)
        competitionS = Competition.objects.get(name='Jicin leto')
        assert adam.get_maximal_allowed_competition_lenght(competitionS) == 25
        self.form(adam, competitionS, 25)
