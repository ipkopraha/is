import datetime

from django.test import TestCase

from main.models import Person, AgeTypes

class TestSwimmerAge(TestCase):
    def setUp(self):
        Person.objects.create(first_name='Adam',
                              sex='Male',
                              birth_date=datetime.date(year=2000,
                                                       month=2,
                                                       day=1)
                              )

    def test_payments_membership(self):
        adam = Person.objects.get(first_name="Adam")
        assert adam.get_swimmerage_type() == 1 # in 2022
        assert adam.get_swimmerage_type(2017) == 0 # regular in 2017
        assert adam.get_swimmerage_type(2071) == 2 #


