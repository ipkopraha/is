## O projektu
Informace budou doplněny:

## Running Locally

go to the project `cd is`
build and run image: `docker-compose up --build`

go to http://127.0.0.1:8000/ to see website or http://127.0.0.1:8000/admin to see admin.

In Docker container use 
```
docker-compose run --rm app sh -c "python manage.py createsuperuser"`
```

### Run unit tests

```console
docker-compose run --rm app sh -c "python manage.py test tests"
```


## Load initial data
```console
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py loaddata /app/test_data/static/2_lengths.json
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py loaddata /app/test_data/static/7_membership_fees.json
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py loaddata /app/test_data/static/8_brigadeeWorks.json
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py loaddata /app/test_data/static/9_money_to_brigade_rate.json
```

## Migrate real data
```console
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_people.py').read())"
docker-compose -f docker-compose-test.yml run  --no-deps -v `pwd`/test_data:/app/test_data -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_payments.py').read())"
```

## Update translations:
```console
docker-compose run --rm app sh -c "django-admin  makemessages -a"
```
Fill out files `conf/locale/cs/LC_MESSAGES/django.po` and `conf/locale/en/LC_MESSAGES/django.po`
Then run:
```console
docker-compose run --rm app sh -c "django-admin compilemessages"
```
and push the results to gitlab.

## log into prod server (docker)
```console
ssh -i C:\Users\izizk/.ssh/id_rsa otuz
```
credential saved in .ssh folder

### check docker images
list running containers
```console
sudo docker ps
```
kill a container
```console
sudo docker stop <contaner-name>
```
restart a container
```console
sudo docker-compose -f docker-compose-prod.yml restart <service>
```
start the whole staff
```console 
sudo docker-compose -f docker-compose-prod.yml up -d
```
logy
```console
sudo docker-compose -f docker-compose-prod.yml logs -f <service>
```
play with debug mode:
```console
sudo DEBUG=1 docker-compose -f docker-compose-prod.yml up -d
```
run a command in the running container:
```console
sudo  docker-compose -f docker-compose-prod.yml exec <service> <command>
```

### Recover Data
List all backups
```console
./gdrive --config ./prod/.gdrive --service-account gdrive-key.json list
```
Download a FileID
```console
./gdrive --config ./prod/.gdrive --service-account gdrive-key.json download <FileID>
```
Download to local PC
```console
scp otuz:dump.json
```
Remove dump.json on server
```console
rm dump.json
```
### Restore data
First flush old database
```console
sudo docker-compose -f docker-compose-prod.yml exec app python manage.py dbshell;
\dt # list tables
TRUNCATE <table_name>, <table_name>, ... CASCADE;
```
Extract data (in this order):
```console
jq '[.[] | select(.model == "contenttypes.contenttype")]' dump.json > contenttypes.contenttype.json
jq '[.[] | select(.model == "auth.permission")]' dump.json > auth.permission.json
jq '[.[] | select(.model == "auth.group")]' dump.json > auth.group.json
jq '[.[] | select(.model == "auth.user")]' dump.json > auth.user.json
```
Load data to database
```console
docker-compose -f docker-compose-test.yml run --rm -v <dir_to_dump_file>:/app/db_export app python manage.py loaddata --format=json "contenttypes.contenttype.json"
docker-compose -f docker-compose-test.yml run --rm -v <dir_to_dump_file>:/app/db_export app python manage.py loaddata --format=json "auth.permission.json"
docker-compose -f docker-compose-test.yml run --rm -v <dir_to_dump_file>:/app/db_export app python manage.py loaddata --format=json "auth.group.json"
docker-compose -f docker-compose-test.yml run --rm -v <dir_to_dump_file>:/app/db_export app python manage.py loaddata --format=json "auth.user.json"
docker-compose -f docker-compose-test.yml run --rm -v <dir_to_dump_file>:/app/db_export app python manage.py loaddata --format=json "dump.json"
```


contenttypes.contenttype
auth.permission
auth.group
auth.user