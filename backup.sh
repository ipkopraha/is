#!/bin/bash

# Load environment variables from .env file
if [ -f ".env" ]; then
  source .env
else
  echo ".env file not found!"
  exit 1
fi

# Name of the container running PostgreSQL
CONTAINER_NAME=prod_db

# Directory to save backups in
BACKUP_DIR="/home/ubuntu/prod/db_export"

# Filename for the backup (with timestamp)
BACKUP_FILENAME="$BACKUP_DIR/backup_$(date +'%Y%m%d%H%M%S').sql"

# Export the password so that pg_dump can use it without prompting
export PGPASSWORD=$DB_PASS

# Run pg_dump inside the container and save the output to a file
docker exec $CONTAINER_NAME pg_dump -U $DB_USER -d $DB_NAME > $BACKUP_FILENAME

# Print out the result (success/failure)
if [ $? -eq 0 ]; then
  echo "Database backup successful!"
  echo "Backup saved to $BACKUP_FILENAME"
else
  echo "Database backup failed."
fi

# Unset the password for security reasons
unset PGPASSWORD
