#!/bin/sh


echo Flushing database
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps app python manage.py flush --noinput
echo Importing all fixtures
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py loaddata "/app/scripts/data/static/2_lengths.json" "/app/scripts/data/static/7_membership_fees.json" "/app/scripts/data/static/8_brigadeeWorks.json" "/app/scripts/data/static/9_money_to_brigade_rate.json" "/app/scripts/data/static/14_groups.json" "/app/scripts/data/initial/11_venues.json" "/app/scripts/data/initial/12_competitions.json"

echo Importing people
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_people.py').read())"
#suod docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_people_rand_password.py').read())"
echo Importing payments
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_payments.py').read())"
echo Importing Brigades
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades.py').read())"
echo Importing Brigades22-23
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades22.py').read())"
echo Importing Brigades23-24
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades23.py').read())"
echo Importing Roles
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_roles.py').read())"
echo Importing Fees
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_update_fees.py').read())"
echo Change payments due in 2023
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/change_due_in_Payments_2023.py').read())"
echo Update Person
sudo docker-compose -f docker-compose-prod.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/person_updates.py').read())"
