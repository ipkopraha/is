import math
from datetime import datetime, date

from django.core.exceptions import ObjectDoesNotExist

import csv

from django.db.models.signals import post_save

from main.models import Person, Payment, MembershipFees
from os import walk

from main.signals import mute_signals_for

print('Deleting Peyments')
Payment.objects.all().delete()
print('Done')

def way_of_transfer(variable):
    if isinstance(variable, float):
        return 'cash'

    if variable.upper() == "B":
        return 'bank'
    if variable.upper() == "H":
        return 'cash'
    else:
        return 'other'


def date2date(d):
    if not isinstance(d, str):
        return date(year=current_year, month=1, day=1)
    if '.' in d:
        return datetime.strptime(d.replace(' ',''), '%d.%m.%Y')
    if '/' in d:
        return datetime.strptime(d.replace(' ',''), '%d/%m/%Y')


_, _, files = next(walk('scripts/data/csvs/'))
files = [f for f in files if 'payments' in f and 'csv' in f]
print(files)

for filename in files:
    print(filename)
    current_year = int(filename.split('_')[1].split('.')[0])

    with open(f"scripts/data/csvs/{filename}", 'r') as file:
        csv_file = csv.DictReader(file)
        for i, row in enumerate(csv_file):
            row = dict(row)

            if row["value"]:
                row['value'] = int(row['value'])

                # print(i, row)
                try:
                    person = Person.objects.get(last_name=row["Přijmení"],
                                                first_name=row["Jméno"],
                                                birth_date__year=int(row["Ročník"]))
                except ObjectDoesNotExist:
                    continue
                #     print('Object does not exist error')
                #     print(f'person = Person.objects.get(last_name={row["Přijmení"]}, first_name={row["Jméno"]}, birth_date__year={int(row["Ročník"])}')

                age_type = person.get_swimmerage_type(from_year=current_year)
                membership_fee = MembershipFees.objects.get(year=current_year, swimmerage_type=age_type)

                paid_amount = row["value"]
                membership_amount = min(paid_amount, membership_fee.amount)
                brigade_amount = max(0, paid_amount - membership_fee.amount)

                if len(row['Datum']) < 7:
                    row['Datum'] = '1.1.' + str(current_year)
                # find out required membership fee

                new_payment = Payment(
                    person=person,
                    due_date=date(year=current_year, month=1, day=1),
                    date=date2date(row['Datum']),
                    membership_amount=membership_amount,
                    brigade_amount=brigade_amount,
                    way_of_transfer=way_of_transfer(row["H/B"])
                )

                with mute_signals_for(new_payment, signals=[post_save]):
                    new_payment.save()
