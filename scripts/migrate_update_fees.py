from main.models import Person
from datetime import date
from tqdm import tqdm

persons = Person.objects.all()

for person in tqdm(persons):
    for year in [2017, 2018, 2019, 2020, 2021, 2022]:
        person.update_fees(date(year, 10, 1))
