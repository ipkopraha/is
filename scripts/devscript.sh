DJANGO_SUPERUSER_PASSWORD=$1

sudo docker-compose exec -T web truncate -s 0 ./app/db.sqlite3
sudo docker-compose exec -T web poetry run python app/manage.py makemigrations --noinput main
sudo docker-compose exec -T web poetry run python app/manage.py migrate --noinput
sudo docker-compose exec -T web poetry run python app/manage.py createsuperuser --no-input --username alfred
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/static/*.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/static/initial_data/*.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/1_users.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/5_persons.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/6_medicalExam.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/10_payments.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/13_registrations.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/14_group.json
sudo docker-compose exec -T web poetry run python app/manage.py loaddata ./app/test_data/15_user_groups.json