import csv
from datetime import datetime

from django.db.models.signals import post_save
from tqdm import tqdm

from main.models import Person, Brigade
from main.signals import mute_signals_for

def date2date(d):
    if '.' in d:
        return datetime.strptime(d, '%d.%m.%Y').date()
    if '/' in d:
        return datetime.strptime(d, '%d/%m/%Y').date()

tomas = Person.objects.filter(last_name="Prokop",
                              first_name="Tomáš",
                              birth_date__year=1975)

with open("scripts/data/csvs/brigades23.csv", 'r') as file:
    csv_file = csv.DictReader(file)

    for i, row in tqdm(enumerate(csv_file)):
        row = dict(row)
        # print(row)
        first_name, last_name, birth_year = row['Jméno'], row['Příjmení'], int(row['Ročník'])

        person = Person.objects.filter(last_name=last_name,
                                       first_name=first_name,
                                       birth_date__year=birth_year)

        if len(person) == 0:
            print(f"Didn't find person with {row}")
            print(Person.objects.filter(last_name=row["Příjmení"]))
            continue
        elif len(person) == 1:
            person = person[0]
        else:
            print(f"Found more then 1 person. {row}")

        print(person, row)
        if row['value'] == '1':
            fulfilled = True
            row['Datum'] = '01.01.2024'
        else:
            fulfilled = False

        if len(row['Hodin'].strip()) > 0 or fulfilled:
            row['Datum'] = '01.01.2024' if len(row['Datum'].strip()) == 0 else row['Datum']

            brigade = Brigade(person=person,
                              date=date2date(row['Datum']),
                              hours=0 if row['Hodin'].strip() == "" else float(row['Hodin']),
                              accountable=tomas[0],
                              fulfilled_differently=fulfilled,
                              verified=True
                              )
            with mute_signals_for(brigade, signals=[post_save]):
                brigade.save()
