from tqdm import tqdm

from main.models import Person
from datetime import date

for person in tqdm(Person.objects.all()):
    for year in [2022, 2023, 2024]:
        person.update_fees(date(year, 1, 2))
        person.update_brigades(date(year, 1, 1))
