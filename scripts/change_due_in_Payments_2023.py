from datetime import date

from tqdm import tqdm

from main.models import Payment

for pay in tqdm(Payment.objects.all()):
    if pay.date.year == 2023:
        print('before',pay.due_date)
        pay.due_date = date(2023, 1, 1)
        pay.save()
        print('after',pay.due_date)
        print(pay)
