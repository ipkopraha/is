#!/bin/sh


echo Flushing database
docker-compose -f docker-compose-test.yml run --rm --no-deps app python manage.py flush --noinput
echo Importing all fixtures
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py loaddata "/app/scripts/data/static/2_lengths.json" "/app/scripts/data/static/7_membership_fees.json" "/app/scripts/data/static/8_brigadeeWorks.json" "/app/scripts/data/static/9_money_to_brigade_rate.json" "/app/scripts/data/static/14_groups.json" "/app/scripts/data/initial/11_venues.json" "/app/scripts/data/initial/12_competitions.json"

echo Importing people
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_people.py').read())"
echo Importing payments
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_payments.py').read())"
echo Importing Brigades
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades.py').read())"
echo Importing Brigades22-23
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades22.py').read())"
echo Importing Roles
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_roles.py').read())"
echo Importing Fees
docker-compose -f docker-compose-test.yml run --rm --no-deps -v `pwd`/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_update_fees.py').read())"
