import csv
from datetime import date

from django.contrib.auth.models import Group

from main.models import Person, Brigade

from django.contrib.auth.models import Group, Permission

from main.models import Person, Brigade

# create roles
permission_to_add = {
    'Competition Manager Role': ['competition', 'competition length', 'competition venue', 'registration'],
    'Medical Examination Role': ['medical examination', ],
    'HR role': ['user', 'person', ],
    'Brigade Manager': ['brigade work', 'brigade'],
    'Treasurer Role': ['membership fees', 'money to brigade rate', 'payment']
}

for group, models in permission_to_add.items():
    new_group, created = Group.objects.get_or_create(name=group)
    for model in models:
        for permission in ['view','add','delete','change']:
            try:
                name = 'Can {} {}'.format(permission, model)
                model_add_perm = Permission.objects.get(name=name)
            except Permission.DoesNotExist:
                print(f'Permission {name} does not exist.')
                continue

            new_group.permissions.add(model_add_perm)



with open("scripts/data/csvs/roles.csv", 'r') as file:
    csv_file = csv.DictReader(file)

    for i, row in enumerate(csv_file):
        row = dict(row)
        # print(row)
        first_name, last_name, birth_year, role = row['Jméno'], row['Příjmení'], int(row['Ročník']), row['role']

        # find corresponding person
        person = Person.objects.get(last_name=last_name,
                                    first_name=first_name,
                                    birth_date__year=birth_year)

        # find corresponding role
        group = Group.objects.get(name=role)
        group.user_set.add(person.user)

        group.save()
        print('saved ', group)
