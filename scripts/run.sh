#!/bin/sh

set -e

ls -la /vol/
ls -la /vol/web

whoami

python manage.py wait_for_db
python manage.py collectstatic --noinput
python manage.py migrate

echo FOLLOWS PROXY PORT, BEWARE!
echo $APP_PORT

uwsgi --socket :$APP_PORT --workers 1 --master --module app.wsgi
