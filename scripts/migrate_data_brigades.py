import csv
from datetime import date

from django.db.models.signals import post_save
from tqdm import tqdm

from main.models import Person, Brigade
from main.signals import mute_signals_for

with open("scripts/data/csvs/brigades.csv", 'r') as file:
    csv_file = csv.DictReader(file)

    for i, row in tqdm(enumerate(csv_file)):
        row = dict(row)
        # print(row)
        first_name, last_name, birth_year, fulfilled = row['Jméno'], row['Příjmení'], int(row['Ročník']), row['value']

        fulfilled = True if fulfilled == "ano" else False

        person = Person.objects.filter(last_name=last_name,
                                       first_name=first_name,
                                       birth_date__year=birth_year)

        if len(person) == 0:
            print(f"Didn't find person with {row}")
            continue
        elif len(person) == 1:
            person = person[0]
        else:
            print(f"Found more then 1 person. {row}")

        brigade = Brigade(person=person,
                          date=date(2022, 1, 1),
                          hours=0,
                          fulfilled_differently=fulfilled,
                          verified=True
                          )
        with mute_signals_for(brigade, signals=[post_save]):
            brigade.save()

        brigade = Brigade(person=person,
                          date=date(2021, 1, 1),
                          hours=0,
                          fulfilled_differently=fulfilled,
                          verified=True
                          )
        with mute_signals_for(brigade, signals=[post_save]):
            brigade.save()

for person in Person.objects.all():
    for year in [2017, 2018, 2019]:
        brigade = Brigade(person=person,
                          date=date(year, 10, 1),
                          hours=0,
                          fulfilled_differently=True,
                          verified=True)
        with mute_signals_for(brigade, signals=[post_save]):
            brigade.save()
