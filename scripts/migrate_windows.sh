#! /bin/sh
p=C:/Users/izizk/Projects/is
docker-compose run --rm --no-deps app python manage.py flush
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/static/2_lengths.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/static/7_membership_fees.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/static/8_brigadeeWorks.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/static/9_money_to_brigade_rate.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/initial/11_venues.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py loaddata "scripts/data/initial/12_competitions.json"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_people.py').read())"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_payments.py').read())"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades.py').read())"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_brigades22.py').read())"
docker-compose run --rm --no-deps -v $p/scripts:/app/scripts app python manage.py shell -c "exec(open('scripts/migrate_data_roles.py').read())"
echo Finished
sleep 20
