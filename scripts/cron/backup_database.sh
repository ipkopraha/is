#!/bin/bash

python manage.py dumpdata --output /tmp/dump.json
/scripts/cron/gdrive --config /app/.gdrive --service-account gdrive-key.json upload /tmp/dump.json
/scripts/cron/gdrive --config /app/.gdrive --service-account gdrive-key.json sync upload /vol/web/media/documents 1hMW27wivN_6T8s8pwQX-tY6UwiaZV995