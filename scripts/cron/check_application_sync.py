from datetime import date
from main.models import Competition, Person, Registration
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.mail import send_mail
import requests
from app.settings import CZECHSWIMMING_USER, CZECHSWIMMING_PASS, CSPS_DOMAIN


def check_competition_applications(competition_id):
    with requests.Session() as s:
        # LOGIN
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}
        login = s.get(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/session',
                      headers=headers)
        login_data = {
            "j_username": CZECHSWIMMING_USER,
            "j_password": CZECHSWIMMING_PASS,
        }
        site = s.post(f'{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/j_security_check',
                      headers=headers, data=login_data, verify=False)

        # download competition details
        url = f"{CSPS_DOMAIN}/cz.zma.csps.portal.rest/api/intranet/competitions/{competition_id}/club-applications/22"
        headers = {
            "accept": "application/json",
        }
        x = s.get(url, headers=headers)
        data = x.json()

        # Extract the required information
        applications = []
        for representative in data['clubRepresents']:
            for discipline in representative['disciplines']:
                length = int(''.join(filter(str.isdigit, discipline['title'])))
                applications.append({
                    'firstname': f"{representative['firstName']}",
                    'lastname':  f"{representative['lastName']}",
                    'birthyear':  f"{representative['birthYear']}",
                    'length': length
                })

        # get all people who applied for the competition
        competition2 = Competition.objects.get(iscsps_id=competition_id)
        registrations = Registration.objects.filter(competition=competition2).order_by('person__last_name')
        # check if all applications are in registrations
        email_text = "Today is the last day for applications to " + str(competition2) + ", therefore we are checking if all applications are synced. Here is the result.\n"
        for registration in registrations:
            person = registration.person
            found = False
            for application in applications:
                if application['firstname'].lower() == person.first_name.lower() and application['lastname'].lower() == person.last_name.lower():
                    for length in registration.length.all():
                        if application['length'] == length.length:
                            found = True
                            application.update({'found': True})
                        break
            if not found:
                email_text += f"Application for person {person} not found in applications on CSPS server!\n"

        # list all applications that were not found
        for application in applications:
            if not application.get('found', False):
                email_text += f"Application on CSPS server {application} is missing on IS I. PKO\n"

        # send email to competition managers
        group = Group.objects.filter(name='Competition Manager Role').first()
        if group is not None:
            all_recipients = group.user_set.all()
            recipient_list = list(Person.objects.filter(user__in=all_recipients).values_list('email', flat=True))
            subject = 'Application sync check for ' + str(competition2)
            email_from = settings.EMAIL_HOST_USER
            send_mail(subject, email_text, email_from, recipient_list)


# get all active competitions, where signing_until is today
competitions = Competition.objects.filter(signing_until=date.today())

for competition in competitions:
    check_competition_applications(competition.iscsps_id)