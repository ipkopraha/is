import datetime
from django.core.mail import send_mail
from django.conf import settings
from main.utils import get_unfulfilled_lists

fulfilled_brigade, fulfilled_memberships = get_unfulfilled_lists(datetime.date.today().year)
data = zip(fulfilled_brigade, fulfilled_memberships)
subject = 'You did not pay'
email_from = settings.EMAIL_HOST_USER
for brigade, membership in data:
    recipient_list = [membership.person.email]
    message = "Warning"
    if not membership.fulfilled_membership:
        message += '\n' + 'you did not pay membership. Pay :' + str(membership.missing_membership_money)
    if not brigade.fulfilled_brigade:
        message += '\n' + 'you did not work enough brigades. Missing hours: ' + str(brigade.missing_brigade_hours) + ' or pay ' + str(brigade.missing_brigade_money)
    message += '\n' + "IS I.PKO"
    # send_mail(subject, message, email_from, recipient_list)
