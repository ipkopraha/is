#!/bin/sh
#
backup_dir=$1
backup_file=$2

docker-compose -f docker-compose-test.yml run --rm -v $backup_dir/db_export:/app/db_export app python manage.py loaddata --format=json "$backup_dir/$backup_file"
