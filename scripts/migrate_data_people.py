import csv
from datetime import datetime
from signal import signal

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from tqdm import tqdm

from main.models import Person, MedicalExamination
from main.signals import mute_signals_for

sex_map = {"Ž": "Female", "M": "Male"}

def date2date(d):
    if '.' in d:
        return datetime.strptime(d, '%d.%m.%Y').date()
    if '/' in d:
        return datetime.strptime(d, '%d/%m/%Y').date()


with open("scripts/data/csvs/people.csv", 'r') as file:
    csv_file = csv.DictReader(file)
    for i, row in tqdm(enumerate(csv_file)):
        row = dict(row)
        if row['Číslo průkazky']:
            row['Číslo průkazky'] = int(row['Číslo průkazky'])
        else:
            row['Číslo průkazky'] = None

        if int(row['Aktivní']) == 0:
            # print(f"Skipping {row}")
            continue

        username = ''
        if not isinstance(row['Primární email'], float) \
                and len(row['Primární email']) > 1 \
                and '@' in row['Primární email']:
            username = row['Primární email']
            email = username
        else:
            username = row['Jméno'] + '_' + row['Přijmení'] + "_" + str(row['Ročník'])
            email = None

        birth_date = date2date(row['narození'])
        if User.objects.filter(username=username).exists():
            print('User exists', User.objects.filter(username=username))
            continue

        user = User.objects.create_user(username, email=email, password=username)

        user.is_staff = True

        user.save()

        person = Person(user=user,
                        first_name=row['Jméno'],
                        last_name=row['Přijmení'],
                        email=email,
                        birth_date=birth_date,
                        RC=row['Rodné číslo'],
                        sex=sex_map[row['M/Ž']],
                        phone_number=row['telefon'],
                        member_since=date2date(row['člen od']),
                        pass_id=None if isinstance(row['Číslo průkazky'], float) else row['Číslo průkazky'],
                        address=row['adresa'],
                        points_id=0,
                        iscsps_id=row['ID IS CSPS'],
                        )

        with mute_signals_for(person, signals=[post_save]):
            person.save()
            # print(f"Added {person}")

        if row['prohlídka do']:
            examination = MedicalExamination(person=person,
                                             verified=True,
                                             examination_until=date2date(row['prohlídka do']))
            with mute_signals_for(examination, signals=[post_save]) :
                examination.save()
