FROM python:3.9-alpine3.13
LABEL maintainer="Karel Durkota"

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ARG APP_PORT="${APP_PORT}"
ENV APP_PORT="${APP_PORT}"
ARG LISTEN_PORT="${LISTEN_PORT}"
ENV LISTEN_PORT="${LISTEN_PORT}"

COPY ./requirements.txt /requirements.txt
COPY ./app /app
COPY ./scripts /scripts

WORKDIR /app
EXPOSE 9002

# Install Chromium and chromedriver for Selenium
RUN apk update && apk add --no-cache chromium chromium-chromedriver

# Set environment variables for headless mode and disable GPU
ENV HEADLESS=true
ENV CHROME_PATH=/usr/bin/chromium-browser
ENV CHROME_DRIVER_PATH=/usr/bin/chromedriver


RUN python -m venv /py && \
    /py/bin/pip install --upgrade pip && \
    apk add --update --no-cache postgresql-client && \
    apk add --update --no-cache --virtual .tmp-deps \
        build-base postgresql-dev musl-dev linux-headers && \
    /py/bin/pip install -r /requirements.txt && \
    apk del .tmp-deps && \
    adduser --disabled-password --no-create-home app && \
    mkdir -p /vol/web/static && \
    mkdir -p /vol/web/media && \
    chown -R app:app /vol && \
    chmod -R 755 /vol && \
    chmod -R +x /scripts

RUN apk add --update --no-cache gettext

ENV PATH="/scripts:/py/bin:$PATH"

USER app

CMD ["run.sh"]

